<?php include('common/header.php'); ?>
<div class="container-fluid">
        <div class="row row-fluid">
            <div data-aos="fade-right" class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("team") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>               
            </div>
        </div>
    </div>

<div class="main-content">
    <div class="container-fluid">
        <div class="row row-fluid pb-12">
            <div class="container">
                <div class="row" data-aos="flip-up">
                    <div class="col-sm-12">
                        <div class="noo-portfolio">
<?php foreach ($adminIndustryClass->getImg() as $image){?>
                            <div class="masonry-item columns-3">
                                <div class="noo-services-item">
                                    <div style="text-align: center;" class="thumb"><a>
                                            <span class="line-left"></span> <span class="line-right"></span>
                                            <img src='<?php echo ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_URL . $image['image']; ?>' />
                                        </a></div>
                                    <h3 style="text-transform: uppercase;"><a><b><?php echo $image['title']; ?></b></a></h3>
                                </div>
                            </div>

<?php }?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('common/footer.php'); ?>