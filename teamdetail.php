<div class="noo-team-wrap noo-team-popup">
<div class="noo-team-content">
<button class="team-remove fa fa-close mfp-close"></button>
<div class="team-left">
<div class="noo-team-image"> <img width="212" height="328" src="images/t1.png" alt="team"> </div>
<div class="noo-team-info">
<h4 class="team_name">R.B Maurya</h4>
<span class="team_position">CEO</span>
</div>
</div>
<div class="team-right">
<div class="team-contact">

<div class="team-specialty">
<h6 class="team-title">Massage</h6>
<p>Lakshmi Trading Company established by Mr. R.B Maurya in the year 1994 is one of the largest importer & stockist of bearings. Situated in the business capital of India.
<br />
Delhi it carries very good name & goodwill in the market.
<br />
Lakshmi Trading Company (Ltc Bearing) primarily deals in bearings of Ina, Ntn, Iko, Mcgill, Tsubaki, Ozak. We keep bearings in wide range useful for various industries & market.
<br />
We also have a showroom to facilitates prompt service & to fulfill the requirements of the masses</p>
</div>

<h6 class="team-title">Contact</h6>
<ul>
<li><i class="fa fa-phone"></i> <span class="team-phone">(+01)-793-7938</span></li>
<li><i class="fa fa-envelope"></i> <a class="team-email">email@domain.com</a></li>
</ul>

</div>
</div>
</div>
</div>
