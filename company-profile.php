<?php include('common/header.php'); ?>

<div class="container-fluid">
        <div data-aos="fade-right" class="row row-fluid">
            <div class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("about") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
                <!-- END REVOLUTION SLIDER -->
            </div>
        </div>
    </div>
<div class="main-content">
    <div class="container-fluid">
        <div class="row row-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 pt-5">
                        <h2 class="intro-title-1" data-aos="fade-left">Company Profile</h2>
                        <h4 class="intro-title-2" data-aos="fade-right">Think "Bearings".......... & U think of us............</h4>
                        <p data-aos="fade-up" class="text-justify">Lakshmi Trading Company was established to be a one stop solution, offering world class bearings through partnering with leading manufacturers across the Globe.
                            <br>
                            Since its inception in 1994, Our Founder Mr. R.B Maurya set upon his dream in the form of Lakshmi Trading & he showed the seeds of leadership, hard-work & commitment.
                            <br>
                            Lakshmi Trading Company also known as (LTC Bearing), Supplier of world renowned bearing brands INA-FAG- Germany, Tsubaki Japan, NSK-RHP- Japan, THK-Japan, MCGILL-USA, IKO-Japan, NTN-Japan, OZAK, Japan, EZO, JAF, ABBA Linear Bearing.
                            <br>
                            Lakshmi Trading Company offers in house brand MNB LTC sources its product range from top producers of bearings worldwide ensures uncompromising quality as per ISO standards and supplies them to customers at competitive prices.
                            <br>
                            Lakshmi Trading Company has become the Largest & Best-known Supplier of Bearings in India. Lakshmi Trading Company has grown rapidly in almost in every segment they operates viz. Machine Tool, Switchgear, Motor & Gearbox, Pharma Machinery, Steel Mill, Laser Machinery, Textile, Power Plant, General Industry, Defence, many more........
                            <br>
                            "Lakshmi Trading Company not only sell bearings, provides solutions to meet customer's requirements."
                            <br>
                            Lakshmi Trading Company believes in quality products and services are the benchmark. Each and every customer is treated as an opportunity and every achievement as a platform to set new goals. This strategy has enabled Lakshmi Trading Company to have delighted customers in India. We are a progressive and innovative company. Our Three fold business motto is Total dedication, Total commitment and Total Quality. Combining it with our passion for applying our learning from business to further enhance our performance we are confident of setting even higher benchmarks for the Indian Bearing Industry in the years to come</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid" data-aos="zoom-in-down">
        <div class="row row-fluid pt-2 pb-2">
            <div class="col-sm-12">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-6 pull-rtl">
                            <div class="blog-title style2">
                                <h2><strong>WHY US</strong></h2>
                                <img src="images/aboutus/about.png" class="img-responsive">
                                <br />
                                <img src="images/aboutus/aboutUs.png" class="img-responsive">

<!--<p>Latest news are on top all times including announcement, special offers, car care tips to make sure this site informative.</p>-->
                                <nav class="noo-blog-control">
                                    <button class="blog-prev">
                                        <i class="fa fa-long-arrow-left"></i>
                                    </button>
                                    <span class="arrow-line"></span>
                                    <button class="blog-next">
                                        <i class="fa fa-long-arrow-right"></i>
                                    </button>
                                </nav>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-6">
                            <div class="row">
                                <ul class="noo_our_blog">
                                    <li class="our-blog-item">
                                        <div class="blog-item style2">
                                            <div class="blog-image" data-image="images/cat.jpg"></div>
                                            <div class="sh-entry-blog"><h3>Our Mission</h3></div>
                                            <div class="blog-hover">
                                                <div class="sh-entry-blog2"><h3><a>Our Mission</a></h3></div>
                                                <p>We endeavor to reach the leadership position in each sector and Service. 
                                                    We are committed to satisfy our customers by providing Quality Product, Service, which gives highest value for money. 
                                                    We commit ourselves to continuous growth, to fulfill the aspirations of our Customer.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="our-blog-item">
                                        <div class="blog-item style2">
                                            <div class="blog-image" data-image="images/cat.jpg"></div>
                                            <div class="sh-entry-blog"><h3>Our Vision</h3></div>
                                            <div class="blog-hover">
                                                <div class="sh-entry-blog2"><h3><a>Our Vision</a></h3></div>
                                                <p>To be a most admired organization in Bearing Industry that enhances the quality of life of all, 
                                                    through sustainable industrial and business development.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="our-blog-item">
                                        <div class="blog-item style2">
                                            <div class="blog-image" data-image="images/cat.jpg"></div>
                                            <div class="sh-entry-blog"><h3>Our Core Values</h3></div>
                                            <div class="blog-hover">
                                                <div class="sh-entry-blog2"><h3><a>Our Core Values</a></h3></div>
                                                <p>
                                                <ul>
                                                    <li>Highest Ethics and Standards</li>
                                                    <li>Passion for customers</li>
                                                    <li>Business Excellence</li>
                                                </ul>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('common/footer.php'); ?>
