<?php include('common/header.php'); ?>
<div class="container-fluid">
        <div class="row row-fluid">
            <div class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("team") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>               
            </div>
        </div>
    </div>

<div id="main" class="main-content container commerce single-product">
<div class="row">
<div class="noo-main col-md-8">
<div class="product">
<div class="noo-product-content">
<div class="images">
<a href="images/pro.gif" class="commerce-main-image zoom" data-rel="prettyPhoto"><img src="images/pro.gif"/></a>
</div>
<div class="summary entry-summary">
<h1 class="product_title entry-title"> Product Name </h1>
<div class="product-description">
<p>Lorem ipsum dolor sit amet, quot inimicus persequeris est ei. Pro eu minimum mentitum, no mei noster dissentiunt. Vel iudico principes efficiendi ut. Soluta labore signiferumque id qui. Te possit moderatius quo, ei nec alterum partiendo.</p>
</div>
</div>
</div>

<div class="commerce-tab-container">
<div class="tabbable commerce-tabs">
<ul class="nav nav-tabs mb-4">
<li class="vc_tta-tab active"> <a data-toggle="tab" href="#tab-1">Details</a> </li>
<li class="vc_tta-tab"> <a data-toggle="tab" href="#tab-2">Send Enquiry</a> </li>
</ul>
<div class="tab-content">
<div id="tab-1" class="tab-pane fade in active">

<table width="100%" border="1">
<tbody>
<tr bgcolor="#00893d">
<td>&nbsp;</td>
<td width="14%" rowspan="2"><strong>Model No.</strong></td>
<td colspan="17" align="center"><strong>Dimensions</strong></td>
</tr>
<tr bgcolor="#00893d">
<td>&nbsp;</td>
<td width="3%"><b>d</b></td>
<td width="3%" align="center"><b>l</b></td>
<td width="7%" align="center"><b>Da</b></td>
<td width="5%" align="center"><b>D</b></td>
<td width="5%" align="center"><b>A</b></td>
<td width="3%" align="center"><b>B</b></td>
<td width="5%" align="center"><b>L</b></td>
<td width="5%" align="center"><b>W</b></td>
<td width="5%" align="center"><b>H</b></td>
<td width="4%" align="center"><b>X</b></td>
<td width="6%" align="center"><b>Y</b></td>
<td width="4%" align="center"><b>Z</b></td>
<td width="5%" align="center"><b>Q</b></td>
<td width="2%" align="center"><b>n</b></td>
<td width="7%" align="center"><b>Ca</b></td>
<td width="8%" align="center"><b>Coa</b></td>
<td width="7%" align="center"><b>K</b></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>DFI1605-4</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">36</td>
</tr>
<tr bgcolor="#DDF0FB">
<td>*</td>
<td>DFI1605-4</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">16</td>
<td align="center">5</td>
<td align="center">36</td>
</tr>
</tbody>
</table>
</div>

<div id="tab-2" class="tab-pane fade">
<div id="comments" class="comments-area">
<div id="respond-wrap">
<div id="respond" class="comment-respond">
<h3 id="reply-title" class="comment-reply-title"> <span>Send Enquiry</span> </h3>
<form class="comment-form">
<div class="row">
<div class="col-sm-6">
<input type="text" placeholder="Enter Your Name*" class="form-control" value="" size="30" />
</div>
<div class="col-sm-6">
<input type="text" placeholder="Enter Your Email*" class="form-control" value="" size="30" />
</div>
<div class="col-sm-6">
<input type="text" placeholder="Enter Your Mobile*" class="form-control" value="" size="30" />
</div>
<div class="col-sm-6">
<input type="text" placeholder="Country*" class="form-control" value="" size="30" />
</div>
<div class="col-sm-12">
<div class="comment-form-comment">
<textarea class="form-control" placeholder="Enter Your Comment" id="comment" name="comment" cols="40" rows="6"></textarea>
</div>
</div>
</div>
<div class="form-submit">
<input name="submit" type="submit" id="submit" class="submit" value="Send Enquiry" />
</div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>
</div>

<div class="noo-sidebar col-md-4">
<div class="noo-sidebar-wrap">
<div class="widget widget_categories">
<h4 class="widget-title">Our Products</h4>
<ul>
<li><a href="#">INA</a></li>
<li><a href="#">CPC</a></li>
<li><a href="#">ABBA</a></li>
<li><a href="#">BD</a></li>
<li><a href="#">IKO</a></li>
<li><a href="#">LM SERIES</a></li>
<li><a href="#">KGB Plain Bearings</a></li>
<li><a href="#">Lubricating Bushes and Bearing</a></li>
</ul>
</div>
</div>
</div>

</div>
</div>

<?php include('common/footer.php'); ?>
