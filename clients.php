<?php include('common/header.php'); ?>

<!--<section class="noo-page-heading">
<div class="container">
<h1 class="page-title">Clinets</h1>
</div>
</section>-->

<div id="main" class="main-content container">
    <div class="row">

        <div class="noo-main col-md-8">
            <div class="noo-portfolio">
<?php foreach ($adminClientsClass->getImg() as $image){?>
                <div class="masonry-item blog-masonry">
                    <div class="our-blog-item">
                        <div class="blog-item">
                            <div style="text-align: center;" class="blog-image"><img width="600" height="450" src='<?php echo ADMIN_CLIENT_UPLOAD_DIR_URL . $image['image'];?> '/></div>
                            <div class="blog-hover">
                                <div class="sh-entry-blog2"><h3><?php echo $image['name']; ?></h3></div>
                            </div>
                        </div>
                    </div>
                </div>

<?php } ?>           
            </div>
        </div>



        <div class="noo-sidebar col-md-4">
            <div class="noo-sidebar-wrap">

                <div class="widget widget_categories">
                    <h4 class="widget-title">Our Products</h4>
                    <ul>
                        <li><a href="#">INA</a></li>
                        <li><a href="#">CPC</a></li>
                        <li><a href="#">ABBA</a></li>
                        <li><a href="#">BD</a></li>
                        <li><a href="#">IKO</a></li>
                        <li><a href="#">LM SERIES</a></li>
                        <li><a href="#">KGB Plain Bearings</a></li>
                        <li><a href="#">Lubricating Bushes and Bearing</a></li>
                    </ul>
                </div>
                <!--<div class="widget widget_noo_best_services">
                <h4 class="widget-title">Best Products</h4>
                <button class="best_services-prev"> <i class="fa fa-long-arrow-left"></i> </button>
                <button class="best_services-next"> <i class="fa fa-long-arrow-right"></i> </button>
                <ul class="best_services">
                <li><a href="#"><img src="images/cat.jpg"/></a></li>
                <li><a href="#"><img src="images/cat.jpg"/></a></li>
                <li><a href="#"><img src="images/cat.jpg"/></a></li>
                </ul>
                </div>-->
            </div>
        </div>

    </div>
</div>

<?php include('common/footer.php'); ?>
