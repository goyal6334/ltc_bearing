<?php include_once('../includes/header.php') ?>
<?php
$events = $adminEventsClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Events</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'event/add.php' ?>" class="btn btn-primary">Add Events</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Event title
                                </th>
                                <th>
                                    Event Image
                                </th>
                                
                                <th>
                                    Start End
                                </th>
                                <th>
                                    End Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($events as $events){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $events[$adminEventsClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $events[$adminEventsClass->title] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_EVENTS_UPLOAD_DIR_URL . $events[$adminEventsClass->image] ?>" alt="<?php echo $events[$adminEventsClass->title] ?>"/>
                                        </td>
                                        
                                         <td>
                                            <?php echo $events[$adminEventsClass->startdate] ?>
                                        </td>
                                        <td>
                                            <?php echo $events[$adminEventsClass->enddate] ?>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$events[$adminEventsClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'event/edit.php?id=' . $events[$adminEventsClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'event/delete.php?id=' . $events[$adminEventsClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>