<?php include_once('../includes/header.php') ?>
<?php
if (!empty($_GET['id'])) {
    $events = $adminEventsClass->getDetailsById($_GET['id']);
    if (empty($events)) {
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'event');
    }

    $title = $events[$adminEventsClass->title];
    $status = $events[$adminEventsClass->status];
    $image = (!empty($events[$adminEventsClass->image])) ? ADMIN_DOWNLOAD_UPLOAD_DIR_URL . $events[$adminEventsClass->image] : "";

    //$startDate = $events[$adminEventsClass->startdate];
    //$endDate = $events[$adminEventsClass->enddate];   
    $startDate = $utility->convertDateIntoNewFormat($events[$adminEventsClass->startdate], 'Y-m-d', 'm/d/Y');


    $endDate = $utility->convertDateIntoNewFormat($events[$adminEventsClass->enddate], 'Y-m-d', 'm/d/Y');
} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'event');
}
if (!empty($_POST)) {
    $errorMsg = "";
    $imagePath = false;
    if (empty($_POST['title'])) {
        $errorMsg = "event title is required.";
    } else {
        if ($_FILES['image']['size'] > 0 && !$_FILES['image']['error']) {

            $targetFilePath = ADMIN_EVENTS_UPLOAD_DIR_PATH;
            if ($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)) {
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if (empty($errorMsg)) {

        if (!empty($_POST['startdate'])) {
            $startDate = $utility->convertDateIntoNewFormat($_POST['startdate'], 'm/d/Y', 'Y-m-d');
        }
        if (!empty($_POST['enddate'])) {
            $endDate = $utility->convertDateIntoNewFormat($_POST['enddate'], 'm/d/Y', 'Y-m-d');
        }

        $data[$adminEventsClass->title] = $_POST['title'];
        if ($imagePath) {
            $data[$adminEventsClass->image] = $imagePath;
        }

        $data[$adminEventsClass->startdate] = $startDate;
        $data[$adminEventsClass->enddate] = $endDate;
        $data[$adminEventsClass->status] = ($_POST['status']) ? 1 : 0;
        if ($result = $adminEventsClass->update($_GET['id'], $data)) {
            if ($result == 2) {
                $errorMsg = "Please input new values before clicking Update";
            } else {
                $configCommon->setFlashSuccess("event updated successfuly.");
                $configCommon->uiRedirect(ADMIN_BASE_URL . 'event');
            }
        } else {
            $errorMsg = "Unable to event serve, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add events</h4>
                </div>
                <div class="card-body">
<?php if (!empty($errorMsg)) { ?>
    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">event Title</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($title) ? $title : "") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Event Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                        </div>
<?php if (!empty($image)) { ?>
                                            <div class="col-md-6">
                                                <img style="width: 100px;" src="<?php echo $image ?>" alt="Event Image"/>
                                            </div>
<?php } ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Start Date</label>
                                    <input type="text" name="startdate" class="form-control" id="date_from" value="<?php echo (!empty($startDate) ? $startDate : "") ?>"/>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">End Date</label>
                                    <input type="text" name="enddate" class="form-control" id="date_to" value="<?php echo (!empty($endDate) ? $endDate : "") ?>"/>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status) ? "checked='checked'" : "") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Event</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>