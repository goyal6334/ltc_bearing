<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $events = $adminEventsClass->getDetailsById($_GET['id']);
    if(empty($events)){
        $configCommon->setFlashError("Invalid download id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
    } else {
        if($adminEventsClass->delete($_GET['id'])){
            if(!empty($events[$adminEventsClass->image])){
                @unlink(ADMIN_DOWNLOAD_UPLOAD_DIR_PATH . $events[$adminEventsClass->image]);
                rmdir(dirname(ADMIN_DOWNLOAD_UPLOAD_DIR_PATH . $events[$adminEventsClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'event');
        } else {
            $configCommon->setFlashError("Unable to delete image, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid download id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'event');
}
?>
