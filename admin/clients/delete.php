<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $client = $adminClientsClass->getDetailsById($_GET['id']);
    if(empty($client)){
        $configCommon->setFlashError("Invalid client id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'clients');
    } else {
        if($adminClientsClass->delete($_GET['id'])){
            if(!empty($client[$adminClientsClass->image])){
                @unlink(ADMIN_CLIENT_UPLOAD_DIR_PATH . $client[$adminClientsClass->image]);
                rmdir(dirname(ADMIN_CLIENT_UPLOAD_DIR_PATH . $client[$adminClientsClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'clients');
        } else {
            $configCommon->setFlashError("Unable to delete client, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid client id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'clients');
}
?>
