<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_POST)){
    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['name'])){
        $errorMsg = "Client name is required.";
    } else {
        if($_FILES['image']['size'] < 1 || $_FILES['image']['error']){
            $errorMsg = "Invalid image to upload.";
        } else {
            $targetFilePath = ADMIN_CLIENT_UPLOAD_DIR_PATH;
            if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if(empty($errorMsg)){
        $data = array(
            $adminClientsClass->name => $_POST['name'],                
            $adminClientsClass->image => $imagePath,
            $adminClientsClass->status => ($_POST['status'])?1:0,
            
        );

        if($adminClientsClass->insert($data)){
            $configCommon->setFlashSuccess("Client created successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'clients');
        } else {
            $errorMsg = "Unable to create client, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add Client</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Client Name</label>
                                    <input type="text" name="name" class="form-control" value="<?php echo (!empty($_POST['name'])?$_POST['name']:"") ?>">
                                </div>
                            </div>
                        </div>
                       
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Client Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($_POST['status'])?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Client</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>