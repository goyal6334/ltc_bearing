<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $client = $adminClientsClass->getDetailsById($_GET['id']);
    if(empty($client)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'clients');
    }

    $name = $client[$adminClientsClass->name];    
    $status = $client[$adminClientsClass->status];
    $image = (!empty($client[$adminClientsClass->image]))?ADMIN_CLIENT_UPLOAD_DIR_URL . $client[$adminClientsClass->image]:"";

} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'clients');
}
if(!empty($_POST)){
    $name = $_POST['name'];    
    $status = $_POST['status'];
    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['name'])){
        $errorMsg = "Client Name is required.";
    } else {
        if($_FILES['image']['size'] > 0 && !$_FILES['image']['error']){
            $targetFilePath = ADMIN_CLIENT_UPLOAD_DIR_PATH;
            if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){

                //Delete old file
                if(!empty($client[$adminClientsClass->image])){
                    @unlink(ADMIN_CLIENT_UPLOAD_DIR_PATH . $client[$adminClientsClass->image]);
                    rmdir(dirname(ADMIN_CLIENT_UPLOAD_DIR_PATH . $client[$adminClientsClass->image]));
                }
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if(empty($errorMsg)){
        $data[$adminClientsClass->name] = $_POST['name'];
        
        if($imagePath){
            $data[$adminClientsClass->image] = $imagePath;
        }
        $data[$adminClientsClass->status] = ($_POST['status'])?1:0;       

        if($adminClientsClass->update($_GET['id'], $data)){
            $configCommon->setFlashSuccess("Client updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'clients');
        } else {
            $errorMsg = "Unable to update client, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Client</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Client Name</label>
                                    <input type="text" name="name" class="form-control" value="<?php echo (!empty($name)?$name:"") ?>">
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Client Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                                </div>
                                                <?php if(!empty($image)){ ?>
                                                    <div class="col-md-6">
                                                        <img style="width: 100px;" src="<?php echo $image ?>" alt="Client Image"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status)?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Client</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>