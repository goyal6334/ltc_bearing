<?php include_once('../includes/header.php') ?>
<?php
$clients = $adminClientsClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Clients</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'clients/add.php' ?>" class="btn btn-primary">Add Clients</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Client Name
                                </th>
                                <th>
                                    Image
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($clients as $client){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $client[$adminClientsClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $client[$adminClientsClass->name] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_CLIENT_UPLOAD_DIR_URL . $client[$adminClientsClass->image] ?>" alt="<?php echo $client[$adminClientsClass->name] ?>"/>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$client[$adminClientsClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'clients/edit.php?id=' . $client[$adminClientsClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'clients/delete.php?id=' . $client[$adminClientsClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>