<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $product = $adminProductClass->getDetailsById($_GET['id']);
    if(empty($product)){
        $configCommon->setFlashError("Invalid product id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'product');
    } else {
        if($adminProductClass->delete($_GET['id'])){
            if(!empty($product[$adminProductClass->image])){
                @unlink(ADMIN_PRODUCTS_UPLOAD_DIR_PATH . $product[$adminProductClass->image]);
                rmdir(dirname(ADMIN_PRODUCTS_UPLOAD_DIR_PATH . $product[$adminProductClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'product');
        } else {
            $configCommon->setFlashError("Unable to delete image, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid product id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'product');
}
?>
