<?php include_once('../includes/header.php') ?>
<?php
$product = $adminProductClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Gallery</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'product/add.php' ?>" class="btn btn-primary">Add Product</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Product Title
                                </th>
                                <th>
                                    Product Image
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($product as $product){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $product[$adminProductClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $product[$adminProductClass->title] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_PRODUCTS_UPLOAD_DIR_URL . $product[$adminProductClass->image] ?>" alt="<?php echo $product[$adminProductClass->title] ?>"/>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$product[$adminProductClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'product/edit.php?id=' . $product[$adminProductClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'product/delete.php?id=' . $product[$adminProductClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>