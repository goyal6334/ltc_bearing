<?php include_once('../includes/header.php') ?>
<?php
if (!empty($_POST)) {
    $errorMsg = "";
    $imagePath = false;
    if (empty($_POST['title'])) {
        $errorMsg = "Image title is required.";
    } else {
        if (empty($_POST['category_id'])) {
            $errorMsg = "Category is required.";
        } else {
            if ($_FILES['image']['size'] < 1 || $_FILES['image']['error']) {
                $errorMsg = "Invalid image to upload.";
            } else {
                $targetFilePath = ADMIN_PRODUCTS_UPLOAD_DIR_PATH;
                if ($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)) {
                    $imagePath = $imageName;
                } else {
                    $errorMsg = "Invalid image to upload.";
                }
            }
        }
    }
    if (empty($errorMsg)) {
        $data = array(
            $adminProductClass->title => $_POST['title'],
            $adminProductClass->content=>$_POST['content'],
            $adminProductClass->image => $imagePath,
            $adminProductClass->category_id => (!empty($_POST['category_id'])) ? $_POST['category_id'] : 0,
            $adminProductClass->status => ($_POST['status']) ? 1 : 0,
        );

        if ($adminProductClass->insert($data)) {
            $configCommon->setFlashSuccess("Product created successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'product');
        } else {
            $errorMsg = "Unable to create product, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add Product</h4>
                </div>
                <div class="card-body">
                    <?php if (!empty($errorMsg)) { ?>
                        <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Product Name</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($_POST['title']) ? $_POST['title'] : "") ?>">
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="category_id" class="form-control">
                                        <?php echo $configCommon->buildSelectOptions($categoryClass->getDropdownList(), "Select Category", ((!empty($_POST['category_id'])) ? $_POST['category_id'] : "")); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <textarea name="content" id="editor" value="<?php echo (!empty($_POST['content']) ? $_POST['content'] : "") ?>"></textarea>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($_POST['status']) ? "checked='checked'" : "") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Image</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>