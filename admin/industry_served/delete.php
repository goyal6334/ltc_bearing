<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $industry_served = $adminIndustryClass->getDetailsById($_GET['id']);
    if(empty($industry_served)){
        $configCommon->setFlashError("Invalid industry id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'industry_served');
    } else {
        if($adminIndustryClass->delete($_GET['id'])){
            if(!empty($industry_served[$adminIndustryClass->image])){
                @unlink(ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_PATH . $industry_served[$adminIndustryClass->image]);
                rmdir(dirname(ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_PATH . $industry_served[$adminIndustryClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'industry_served');
        } else {
            $configCommon->setFlashError("Unable to delete industry, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid image id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'industry_served');
}
?>
