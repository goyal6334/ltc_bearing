<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $industry_served = $adminIndustryClass->getDetailsById($_GET['id']);
    if(empty($industry_served)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'industry_served');
    }

    $title = $industry_served[$adminIndustryClass->title];
    $status = $industry_served[$adminIndustryClass->status];
    $image = (!empty($industry_served[$adminIndustryClass->image]))?ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_URL . $industry_served[$adminIndustryClass->image]:"";

} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'industry_served');
}
if(!empty($_POST)){
    $title = $_POST['title'];
    $status = $_POST['status'];

    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['title'])){
        $errorMsg = "Industry name is required.";
    } else {
        if($_FILES['image']['size'] > 0 && !$_FILES['image']['error']){
            $targetFilePath = ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_PATH;
            if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){

                //Delete old file
                if(!empty($industry_served[$adminIndustryClass->image])){
                    @unlink(ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_PATH . $industry_served[$adminIndustryClass->image]);
                    rmdir(dirname(ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_PATH . $industry_served[$adminIndustryClass->image]));
                }
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if(empty($errorMsg)){
        $data[$adminIndustryClass->title] = $_POST['title'];
        if($imagePath){
            $data[$adminIndustryClass->image] = $imagePath;
        }
        $data[$adminIndustryClass->status] = ($_POST['status'])?1:0;

        if($adminIndustryClass->update($_GET['id'], $data)){
            $configCommon->setFlashSuccess("Industries updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'industry_served');
        } else {
            $errorMsg = "Unable to update Industries, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Industry Served</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Industry Name</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($title)?$title:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Gallery Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                                </div>
                                                <?php if(!empty($image)){ ?>
                                                    <div class="col-md-6">
                                                        <img style="width: 100px;" src="<?php echo $image ?>" alt="Industry Image"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status)?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Industry</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>