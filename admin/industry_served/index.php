<?php include_once('../includes/header.php') ?>
<?php
$industry_served = $adminIndustryClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Industries</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'industry_served/add.php' ?>" class="btn btn-primary">Add Industries</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Industries Name
                                </th>
                                <th>
                                    Industries Image
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($industry_served as $industry_served){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $industry_served[$adminIndustryClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $industry_served[$adminIndustryClass->title] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_URL . $industry_served[$adminIndustryClass->image] ?>" alt="<?php echo $industry_served[$adminIndustryClass->title] ?>"/>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$industry_served[$adminIndustryClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'industry_served/edit.php?id=' . $industry_served[$adminIndustryClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'industry_served/delete.php?id=' . $industry_served[$adminIndustryClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>