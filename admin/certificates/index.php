<?php include_once('../includes/header.php') ?>
<?php
$certificates = $adminCertificatesClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Certificates</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'certificates/add.php' ?>" class="btn btn-primary">Add Certificates</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Certificate Title
                                </th>
                                <th>
                                    Certificate Image
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($certificates as $certificates){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $certificates[$adminCertificatesClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $certificates[$adminCertificatesClass->name] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_CERTIFICATES_UPLOAD_DIR_URL . $certificates[$adminCertificatesClass->image] ?>" alt="<?php echo $certificates[$adminCertificatesClass->name] ?>"/>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$certificates[$adminCertificatesClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'certificates/edit.php?id=' . $certificates[$adminCertificatesClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'certificates/delete.php?id=' . $certificates[$adminCertificatesClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>