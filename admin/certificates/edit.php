<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $certificates = $adminCertificatesClass->getDetailsById($_GET['id']);
    if(empty($certificates)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'certificates');
    }

    $name = $certificates[$adminCertificatesClass->name];
    $status = $certificates[$adminCertificatesClass->status];
    $image = (!empty($certificates[$adminCertificatesClass->image]))?ADMIN_CERTIFICATES_UPLOAD_DIR_URL . $certificates[$adminCertificatesClass->image]:"";

} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'certificates');
}
if(!empty($_POST)){
    $name = $_POST['name'];
    $status = $_POST['status'];

    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['name'])){
        $errorMsg = "Certicates title is required.";
    } else {
        if($_FILES['image']['size'] > 0 && !$_FILES['image']['error']){
            $targetFilePath = ADMIN_CERTIFICATES_UPLOAD_DIR_PATH;
            if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){

                //Delete old file
                if(!empty($certificates[$adminCertificatesClass->image])){
                    @unlink(ADMIN_CERTIFICATES_UPLOAD_DIR_PATH . $certificates[$adminCertificatesClass->image]);
                    rmdir(dirname(ADMIN_CERTIFICATES_UPLOAD_DIR_PATH . $certificates[$adminCertificatesClass->image]));
                }
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if(empty($errorMsg)){
        $data[$adminCertificatesClass->name] = $_POST['name'];
        if($imagePath){
            $data[$adminCertificatesClass->image] = $imagePath;
        }
        $data[$adminCertificatesClass->status] = ($_POST['status'])?1:0;

        if($adminCertificatesClass->update($_GET['id'], $data)){
            $configCommon->setFlashSuccess("Certificate updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'certificates');
        } else {
            $errorMsg = "Unable to update Certificate, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Certificates</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Certificate Title</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($name)?$name:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Certificate Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                                </div>
                                                <?php if(!empty($image)){ ?>
                                                    <div class="col-md-6">
                                                        <img style="width: 100px;" src="<?php echo $image ?>" alt="Certificate Image"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status)?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Certificate</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>