<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $certificates = $adminCertificatesClass->getDetailsById($_GET['id']);
    if(empty($certificates)){
        $configCommon->setFlashError("Invalid certificate id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'certificates');
    } else {
        if($adminCertificatesClass->delete($_GET['id'])){
            if(!empty($certificates[$adminCertificatesClass->image])){
                @unlink(ADMIN_CERTIFICATES_UPLOAD_DIR_PATH . $certificates[$adminCertificatesClass->image]);
                rmdir(dirname(ADMIN_CERTIFICATES_UPLOAD_DIR_PATH . $certificates[$adminCertificatesClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'certificates');
        } else {
            $configCommon->setFlashError("Unable to delete image, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid image id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'certificates');
}
?>
