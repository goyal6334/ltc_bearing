<?php include_once('includes/header.php') ?>
<?php
$errorMsg = array();
$email = '';

if(!empty($_POST)){
    $email = $_POST['email'];
    $password = $_POST['password'];

    if(empty($email)){
        $errorMsg[] = "Email address is required to login.";
    } else {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
           $errorMsg[] = "This is not a valid email address";
        }
    }

    if(empty($password)){
        $errorMsg[] = "Password field is required.";
    }

    if(empty($errorMsg)){
        if($user = $adminUserClass->verifyEmail($email)){
            if($adminUserClass->verifyPassword($user->password, $password)){
                if($adminLoginClass->setLoginUser($user)){
                    echo '<script>self.location = "' . ADMIN_BASE_URL . '";</script>';
                    exit();
                } else {
                    $errorMsg[] = "Something went wrong, unable to login in admin panel.";
                }
            } else {
                $errorMsg[] = "Wrong password, please contact to super admin.";
            }
        } else {
            $errorMsg[] = "User is not exist with this email.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-8">
            <?php if(!empty($errorMsg)){ ?>
                <?php foreach($errorMsg as $msg){ ?>
                    <div class="alert alert-danger">
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <i class="material-icons">close</i>
                      </button>
                      <span>
                        <b> Error - </b> <?php echo $msg ?></span>
                    </div>
                <?php } ?>
            <?php } ?>
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Admin Login</h4>
                </div>
                <div class="card-body">
                    <form name="admin_login_form" method="post">
                        <div class="row">
                            <div class="col-sm-12">&nbsp;</div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email</label>
                                    <input name="email" type="text" class="form-control" value="<?php echo $email ?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Password</label>
                                    <input name="password" type="password" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">&nbsp;</div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Login</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('includes/footer.php') ?>