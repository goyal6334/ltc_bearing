<?php include_once('../includes/header.php') ?>
<?php
$categories = $categoryClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Category</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'categories/add.php' ?>" class="btn btn-primary">Add Category</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Category Name
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($categories as $category){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $category[$categoryClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $categoryClass->getCategoryName($category[$categoryClass->id]) ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'categories/edit.php?id=' . $category[$categoryClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'categories/delete.php?id=' . $category[$categoryClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>