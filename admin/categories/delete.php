<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $category = $categoryClass->getDetailsById($_GET['id']);
    if(empty($category)){
        $configCommon->setFlashError("Invalid category id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'categories');
    } else {
        if($categoryClass->delete($_GET['id'])){
            $configCommon->setFlashSuccess("Category deleted successfully.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'categories');
        } else {
            $configCommon->setFlashError("Unable to delete category, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid category id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'categories');
}
?>
