<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $category = $categoryClass->getDetailsById($_GET['id']);
    if(empty($category)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'categories');
    }

    $name = $category[$categoryClass->name];
    $parentId = $category[$categoryClass->parent_id];

} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'categories');
}
if(!empty($_POST)){
    $name = $_POST['name'];
    $parentId = $_POST['parent_id'];

    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['name'])){
        $errorMsg = "Category name is required.";
    }

    if(empty($errorMsg)){
        $data = array(
            $categoryClass->parent_id => (!empty($_POST['parent_id']))?$_POST['parent_id']:0,
            $categoryClass->name => $_POST['name'],
        );

        if($categoryClass->update($_GET['id'], $data)){
            $configCommon->setFlashSuccess("Category updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'categories');
        } else {
            $errorMsg = "Unable to update category, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Category</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Category Name</label>
                                    <input type="text" name="name" class="form-control" value="<?php echo (!empty($name)?$name:"") ?>">
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <select name="parent_id" class="form-control">
                                            <?php echo $configCommon->buildSelectOptions($categoryClass->getDropdownList($category[$categoryClass->id]), "Select Parent Category", $parentId); ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        <br>
                        <button type="submit" class="btn btn-primary pull-right">Add Category</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>