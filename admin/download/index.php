<?php include_once('../includes/header.php') ?>
<?php
$download = $adminDownloadsClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Downloads</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'download/add.php' ?>" class="btn btn-primary">Add Downloads</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Download title
                                </th>
                                <th>
                                    Downloaded Image
                                </th>
                                <th>
                                    URL
                                </th>
                                <th>
                                    Start End
                                </th>
                                <th>
                                    End Date
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($download as $download){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $download[$adminDownloadsClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $download[$adminDownloadsClass->title] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_DOWNLOAD_UPLOAD_DIR_URL . $download[$adminDownloadsClass->image] ?>" alt="<?php echo $download[$adminDownloadsClass->title] ?>"/>
                                        </td>
                                         <td>
                                            <?php echo $download[$adminDownloadsClass->Url] ?>
                                        </td>
                                         <td>
                                            <?php echo $download[$adminDownloadsClass->startdate] ?>
                                        </td>
                                        <td>
                                            <?php echo $download[$adminDownloadsClass->enddate] ?>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$download[$adminDownloadsClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'download/edit.php?id=' . $download[$adminDownloadsClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'download/delete.php?id=' . $download[$adminDownloadsClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>