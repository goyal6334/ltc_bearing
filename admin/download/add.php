<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_POST)){
    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['title'])){
        $errorMsg = "Download title is required.";
    } else {
        if(empty($_POST['url'])){
            $errorMsg = "Download url is required.";
        } else {
            if (!filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
                $errorMsg = "Please enter valid url.";
            } else {
                if($_FILES['image']['size'] < 1 || $_FILES['image']['error']){
                    $errorMsg = "Invalid image to upload.";
                } else {
                    $targetFilePath = ADMIN_DOWNLOAD_UPLOAD_DIR_PATH;
                    if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){
                        $imagePath = $imageName;
                    } else {
                        $errorMsg = "Invalid image to upload.";
                    }
                }
            }
        }
    }

    if(empty($errorMsg)){
        $startDate = NULL;
        if(!empty($_POST['startdate'])){
            $startDate = $utility->convertDateIntoNewFormat($_POST['startdate'], 'm/d/Y', 'Y-m-d');
        }

        $endDate = NULL;
        if(!empty($_POST['enddate'])){
            $endDate = $utility->convertDateIntoNewFormat($_POST['enddate'], 'm/d/Y', 'Y-m-d');
        }

        $data = array(
            $adminDownloadsClass->title => $_POST['title'],
            $adminDownloadsClass->image => $imagePath,
            $adminDownloadsClass->Url => ($_POST['url']),
            $adminDownloadsClass->startdate => $startDate,
            $adminDownloadsClass->enddate => $endDate,
            $adminDownloadsClass->status => ($_POST['status'])?1:0,
        );

        if($adminDownloadsClass->insert($data)){
            $configCommon->setFlashSuccess("Download inserted successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
        } else {
            $errorMsg = "Unable to download serve, please try again.";
        }
    }
}

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add downloads</h4>
                </div>
                <div class="card-body">
                    <?php if(!empty($errorMsg)){ ?>
                        <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Download Title</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($_POST['title'])?$_POST['title']:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Downloaded Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Start Date</label>
                                    <input type="text" name="startdate" class="form-control" id="date_from" value="<?php echo (!empty($_POST['startdate'])?$_POST['startdate']:"") ?>"/>
                                </div>
                            </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">End Date</label>
                                    <input type="text" name="enddate" class="form-control" id="date_to" value="<?php echo (!empty($_POST['enddate'])?$_POST['enddate']:"") ?>"/>
                                </div>
                            </div>
                        </div>
                        <br>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">URL </label>
                                    <input type="text" name="url" class="form-control" value="<?php echo (!empty($_POST['url'])?$_POST['url']:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($_POST['status'])?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Downloads</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>