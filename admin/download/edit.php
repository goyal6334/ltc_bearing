<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $download = $adminDownloadsClass->getDetailsById($_GET['id']);
    if(empty($download)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
    }
    $url = $download[$adminDownloadsClass->Url];
    $title = $download[$adminDownloadsClass->title];
    $status = $download[$adminDownloadsClass->status];
    $image = (!empty($download[$adminDownloadsClass->image]))?ADMIN_DOWNLOAD_UPLOAD_DIR_URL . $download[$adminDownloadsClass->image]:"";
     
    //$startDate = $download[$adminDownloadsClass->startdate];
    //$endDate = $download[$adminDownloadsClass->enddate];   
    $startDate = $utility->convertDateIntoNewFormat($download[$adminDownloadsClass->startdate],'Y-m-d', 'm/d/Y');
    
    
    $endDate = $utility->convertDateIntoNewFormat($download[$adminDownloadsClass->enddate],'Y-m-d', 'm/d/Y');
   
} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
}
if(!empty($_POST)){
    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['title'])){
        $errorMsg = "Download title is required.";
    } else {
        if(empty($_POST['url'])){
            $errorMsg = "Download url is required.";
        } else {
            if (!filter_var($_POST['url'], FILTER_VALIDATE_URL)) {
                $errorMsg = "Please enter valid url.";
            } else {
               
               if($_FILES['image']['size'] > 0 && !$_FILES['image']['error']){
                    
                    $targetFilePath = ADMIN_DOWNLOAD_UPLOAD_DIR_PATH;
                    if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){
                        $imagePath = $imageName;
                    } else {
                        $errorMsg = "Invalid image to upload.";
                    }
                }
            }
        }
    }

    if(empty($errorMsg)){
       
 if(!empty($_POST['startdate'])){
            $formeatedStartDate = $utility->convertDateIntoNewFormat($_POST['startdate'], 'm/d/Y', 'Y-m-d');
        }
        if(!empty($_POST['enddate'])){
            $formatedEndDate = $utility->convertDateIntoNewFormat($_POST['enddate'], 'm/d/Y', 'Y-m-d');
        }

        $data[$adminDownloadsClass->title] = $_POST['title'];
        if($imagePath){
            $data[$adminDownloadsClass->image] = $imagePath;
        }
        $data[$adminDownloadsClass->Url] = ($_POST['url']);
        $data[$adminDownloadsClass->startdate] = $formeatedStartDate;
        $data[$adminDownloadsClass->enddate] = $formatedEndDate;
        $data[$adminDownloadsClass->status] = ($_POST['status'])?1:0;
           
        if($result = $adminDownloadsClass->update($_GET['id'], $data)){
            if((int)$result == 2){
                $errorMsg = "Please input new values before clicking Update";
            } else {
                $configCommon->setFlashSuccess("Download updated successfuly.");
                $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
            }
        } else {
            $errorMsg = "Unable to download serve, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add downloads</h4>
                </div>
                <div class="card-body">
                    <?php if(!empty($errorMsg)){ ?>
                        <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Download Title</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($title)?$title:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Downloaded Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                        </div>
                                         <?php if(!empty($image)){ ?>
                                                    <div class="col-md-6">
                                                        <img style="width: 100px;" src="<?php echo $image ?>" alt="Downloaded Image"/>
                                                    </div>
                                                <?php } ?>
                                    
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">Start Date</label>
                                    <input type="text" name="startdate" class="form-control" id="date_from" value="<?php echo (!empty($startDate)?$startDate:"") ?>"/>
                                </div>
                            </div>
                              <div class="col-md-6">
                                <div class="form-group">
                                    <label class="">End Date</label>
                                    <input type="text" name="enddate" class="form-control" id="date_to" value="<?php echo (!empty($endDate)?$endDate:"") ?>"/>
                                </div>
                            </div>
                        </div>
                        <br>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">URL </label>
                                    <input type="text" name="url" class="form-control" value="<?php echo (!empty($url)?$url:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status)?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Downloads</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>