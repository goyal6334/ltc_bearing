<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $download = $adminDownloadsClass->getDetailsById($_GET['id']);
    if(empty($download)){
        $configCommon->setFlashError("Invalid download id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
    } else {
        if($adminDownloadsClass->delete($_GET['id'])){
            if(!empty($download[$adminDownloadsClass->image])){
                @unlink(ADMIN_DOWNLOAD_UPLOAD_DIR_PATH . $download[$adminDownloadsClass->image]);
                rmdir(dirname(ADMIN_DOWNLOAD_UPLOAD_DIR_PATH . $download[$adminDownloadsClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
        } else {
            $configCommon->setFlashError("Unable to delete image, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid download id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'download');
}
?>
