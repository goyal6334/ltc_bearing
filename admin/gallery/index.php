<?php include_once('../includes/header.php') ?>
<?php
$galleries = $adminGalleryClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Gallery</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'gallery/add.php' ?>" class="btn btn-primary">Add Gallery</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Gallery Title
                                </th>
                                <th>
                                    Gallery Image
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($galleries as $gallery){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $gallery[$adminGalleryClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $gallery[$adminGalleryClass->title] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_GALLERY_UPLOAD_DIR_URL . $gallery[$adminGalleryClass->image] ?>" alt="<?php echo $gallery[$adminGalleryClass->title] ?>"/>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$gallery[$adminGalleryClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'gallery/edit.php?id=' . $gallery[$adminGalleryClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'gallery/delete.php?id=' . $gallery[$adminGalleryClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>