<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $gallery = $adminGalleryClass->getDetailsById($_GET['id']);
    if(empty($gallery)){
        $configCommon->setFlashError("Invalid gallery id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'gallery');
    } else {
        if($adminGalleryClass->delete($_GET['id'])){
            if(!empty($gallery[$adminGalleryClass->image])){
                @unlink(ADMIN_GALLERY_UPLOAD_DIR_PATH . $gallery[$adminGalleryClass->image]);
                rmdir(dirname(ADMIN_GALLERY_UPLOAD_DIR_PATH . $gallery[$adminGalleryClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'gallery');
        } else {
            $configCommon->setFlashError("Unable to delete image, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid image id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'gallery');
}
?>
