<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $gallery = $adminGalleryClass->getDetailsById($_GET['id']);
    if(empty($gallery)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'gallery');
    }

    $title = $gallery[$adminGalleryClass->title];
    $status = $gallery[$adminGalleryClass->status];
    $image = (!empty($gallery[$adminGalleryClass->image]))?ADMIN_GALLERY_UPLOAD_DIR_URL . $gallery[$adminGalleryClass->image]:"";

} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'gallery');
}
if(!empty($_POST)){
    $title = $_POST['title'];
    $status = $_POST['status'];

    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['title'])){
        $errorMsg = "Gallery title is required.";
    } else {
        if($_FILES['image']['size'] > 0 && !$_FILES['image']['error']){
            $targetFilePath = ADMIN_GALLERY_UPLOAD_DIR_PATH;
            if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){

                //Delete old file
                if(!empty($gallery[$adminGalleryClass->image])){
                    @unlink(ADMIN_GALLERY_UPLOAD_DIR_PATH . $gallery[$adminGalleryClass->image]);
                    rmdir(dirname(ADMIN_GALLERY_UPLOAD_DIR_PATH . $gallery[$adminGalleryClass->image]));
                }
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if(empty($errorMsg)){
        $data[$adminGalleryClass->title] = $_POST['title'];
        if($imagePath){
            $data[$adminGalleryClass->image] = $imagePath;
        }
        $data[$adminGalleryClass->status] = ($_POST['status'])?1:0;

        if($adminGalleryClass->update($_GET['id'], $data)){
            $configCommon->setFlashSuccess("Gallery updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'gallery');
        } else {
            $errorMsg = "Unable to update gallery, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Gallery</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Gallery Title</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($title)?$title:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Gallery Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                                </div>
                                                <?php if(!empty($image)){ ?>
                                                    <div class="col-md-6">
                                                        <img style="width: 100px;" src="<?php echo $image ?>" alt="Gallery Image"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status)?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Gallery</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>