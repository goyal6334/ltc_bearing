<?php include_once('../includes/header.php') ?>
<?php
if (!empty($_POST)) {
    $errorMsg = "";
    $imagePath = false;
    if (empty($_POST['title'])) {
        $errorMsg = "Page title is required.";
    } else {
        if (empty($_POST['type'])) {
            $errorMsg = "Page type is required.";
        } else {
            if (empty($_POST['content'])) {
                $errorMsg = "Page content is required.";
            }
        }
    }
    if (empty($errorMsg)) {
        $data = array(
            $adminPageClass->title => $_POST['title'],
            $adminPageClass->content=>$_POST['content'],
            $adminPageClass->type => (!empty($_POST['type'])) ? $_POST['type'] : 0,
            $adminPageClass->status => ($_POST['status']) ? 1 : 0,
        );

        if ($adminPageClass->insert($data)) {
            $configCommon->setFlashSuccess("Page created successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'pages');
        } else {
            $errorMsg = "Unable to create page, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add Page</h4>
                </div>
                <div class="card-body">
                    <?php if (!empty($errorMsg)) { ?>
                        <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Page Title</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo (!empty($_POST['title']) ? $_POST['title'] : "") ?>">
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="type" class="form-control">
                                        <?php echo $configCommon->buildSelectOptions($configCommon->getPageType(), "Select Page Type", ((!empty($_POST['type'])) ? $_POST['type'] : "")); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <textarea name="content" id="editor" value="<?php echo (!empty($_POST['content']) ? $_POST['content'] : "") ?>"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($_POST['status']) ? "checked='checked'" : "") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Page</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>