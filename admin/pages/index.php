<?php include_once('../includes/header.php') ?>
<?php
$pages = $adminPageClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Pages</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'pages/add.php' ?>" class="btn btn-primary">Add Page</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Product Title
                                </th>
                                <th>
                                    Product Type
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($pages as $page){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $page[$adminPageClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo $page[$adminPageClass->title] ?>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$pageTypeArray[$page[$adminPageClass->type]] ?>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$page[$adminPageClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'pages/edit.php?id=' . $page[$adminPageClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'pages/delete.php?id=' . $page[$adminPageClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>