<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $page = $adminPageClass->getDetailsById($_GET['id']);
    if(empty($page)){
        $configCommon->setFlashError("Invalid page id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'pages');
    } else {
        if($adminPageClass->delete($_GET['id'])){
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'pages');
        } else {
            $configCommon->setFlashError("Unable to delete page, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid page id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'pages');
}
?>
