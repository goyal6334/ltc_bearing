<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $page = $adminPageClass->getDetailsById($_GET['id']);
    if(empty($page)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'pages');
    }

    $title = $page[$adminPageClass->title];
    $content = $page[$adminPageClass->content];
    $type = $page[$adminPageClass->type];
    $status = $page[$adminPageClass->status];

} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'pages');
}
if (!empty($_POST)) {
    $title = $_POST['title'];
    $content = $_POST['content'];
    $type = $_POST['type'];
    $status = $_POST['status'];

    $errorMsg = "";
    $imagePath = false;
    if (empty($_POST['title'])) {
        $errorMsg = "Page title is required.";
    } else {
        if (empty($_POST['content'])) {
            $errorMsg = "Page content is required.";
        } else {
            if (empty($_POST['type'])) {
                $errorMsg = "Page type is required.";
            }
        }
    }
    if (empty($errorMsg)) {
        $data = array(
            $adminPageClass->title => $_POST['title'],
            $adminPageClass->content =>$_POST['content'],
            $adminPageClass->type => (!empty($_POST['type'])) ? $_POST['type'] : 0,
            $adminPageClass->status => ($_POST['status']) ? 1 : 0,
        );

        if ($adminPageClass->update($_GET['id'], $data)) {
            $configCommon->setFlashSuccess("Page updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'pages');
        } else {
            $errorMsg = "Unable to update page, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Page</h4>
                </div>
                <div class="card-body">
                    <?php if (!empty($errorMsg)) { ?>
                        <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Page Title</label>
                                    <input type="text" name="title" class="form-control" value="<?php echo ((!empty($title)) ? $title : "") ?>">
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="type" class="form-control">
                                        <?php echo $configCommon->buildSelectOptions($configCommon->getPageType(), "Select Page Type", ((!empty($type)) ? $type : "")); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea name="content" id="editor">
                                       <?php echo (!empty($content) ? $content : "") ?>
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status) ? "checked='checked'" : "") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Update Page</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>