<?php include_once 'config.php';?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo ADMIN_BASE_URL ?>assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="<?php echo ADMIN_BASE_URL ?>assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Ltc Bearing Admin
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/themes/smoothness/jquery-ui.css" />
        <!-- CSS Files -->
        <link href="<?php echo ADMIN_BASE_URL ?>assets/css/material-dashboard.css?v=2.1.0" rel="stylesheet" />
<!--        <link href="<?php echo ADMIN_BASE_URL ?>assets/css/ckeditor-samples.css" rel="stylesheet" />-->
        <link href="<?php echo ADMIN_BASE_URL ?>assets/css/neo.css" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="<?php echo ADMIN_BASE_URL ?>assets/demo/demo.css" rel="stylesheet" />
    </head>

    <body class="dark-edition">
        <div class="wrapper ">
            <?php if($adminLoginClass->hasLogin()){ ?>
                <?php include_once 'sidebar.php'; ?>
            <?php } ?>
            <div class="main-panel">
                <!-- Navbar -->
                <?php if($adminLoginClass->hasLogin()){ ?>
                    <?php include_once 'menu.php'; ?>
                <?php } ?>
                <!-- End Navbar -->

                <div class="content">
                <?php
                    if(!empty($configCommon->getFlashSuccess())){
                        echo '<div class="alert alert-success alert-dismissible"><a href="#" style="line-height: 40px;" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Success!</strong> ' . $configCommon->getFlashSuccess() . '</div>';
                    }
                    if(!empty($configCommon->getFlashError())){
                        echo '<div class="alert alert-danger alert-dismissible"><a href="#" style="line-height: 40px;" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Error!</strong> ' . $configCommon->getFlashError() . '</div>';
                    }
                    $configCommon->resetFlash();
                ?>
