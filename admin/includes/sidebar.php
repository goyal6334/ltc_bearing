<?php
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$dashboard = false;
$pages = false;
$homeBanner = false;
$gallery = false;
$clients = false;
$industry_served = false;
$download = false;

if($actual_link == ADMIN_BASE_URL . "index.php" || $actual_link == ADMIN_BASE_URL . ""){
    $dashboard = true;
} else if($actual_link == ADMIN_BASE_URL . "pages/index.php" || $actual_link == ADMIN_BASE_URL . "pages/"){
    $pages = true;
} else if($actual_link == ADMIN_BASE_URL . "home_banners/add.php" || $actual_link == ADMIN_BASE_URL . "home_banners/index.php" || $actual_link == ADMIN_BASE_URL . "home_banners/"){
    $homeBanner = true;
} else if($actual_link == ADMIN_BASE_URL . "gallery/add.php" || $actual_link == ADMIN_BASE_URL . "gallery/index.php" || $actual_link == ADMIN_BASE_URL . "gallery/"){
    $gallery = true;
} else if($actual_link == ADMIN_BASE_URL . "clients/add.php" || $actual_link == ADMIN_BASE_URL . "clients/index.php" || $actual_link == ADMIN_BASE_URL . "clients/"){
    $clients = true;
} else if($actual_link == ADMIN_BASE_URL . "industry_served/add.php" || $actual_link == ADMIN_BASE_URL . "industry_served/index.php" || $actual_link == ADMIN_BASE_URL . "industry_served/"){
    $industry_served = true;
}else if($actual_link == ADMIN_BASE_URL . "download/add.php" || $actual_link == ADMIN_BASE_URL . "download/index.php" || $actual_link == ADMIN_BASE_URL . "download/"){
    $download = true;
}
else if($actual_link == ADMIN_BASE_URL . "settings/add.php" || $actual_link == ADMIN_BASE_URL . "settings/index.php" || $actual_link == ADMIN_BASE_URL . "settings/"){
    $settings = true;
}
else if($actual_link == ADMIN_BASE_URL . "certificates/add.php" || $actual_link == ADMIN_BASE_URL . "certificates/index.php" || $actual_link == ADMIN_BASE_URL . "certificates/"){
    $certificates = true;
}
else if($actual_link == ADMIN_BASE_URL . "event/add.php" || $actual_link == ADMIN_BASE_URL . "event/index.php" || $actual_link == ADMIN_BASE_URL . "event/"){
    $events = true;
}
else if($actual_link == ADMIN_BASE_URL . "product/add.php" || $actual_link == ADMIN_BASE_URL . "product/index.php" || $actual_link == ADMIN_BASE_URL . "product/"){
    $product = true;
}
?>
<div class="sidebar" data-color="purple" data-background-color="black" data-image="../assets/img/sidebar-2.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"
 ̰
      Tip 2: you can also add an image using data-image tag
    -->
    <div class="logo"><a href="http://www.creative-tim.com" class="simple-text logo-normal">
            Creative Tim
        </a></div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li class="nav-item <?php echo ($dashboard)?"active":"" ?>">
                <a class="nav-link" href="./dashboard.html">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($category)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>categories/">
                    <i class="material-icons">Category</i>
                    <p>Category</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($product)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>product/">
                    <i class="material-icons">Products</i>
                    <p>Products</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($pages)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>pages/">
                    <i class="material-icons">person</i>
                    <p>Pages</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($homeBanner)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>home_banners/">
                    <i class="material-icons">person</i>
                    <p>Banners</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($gallery)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>gallery/">
                    <i class="material-icons">person</i>
                    <p>Gallery</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($clients)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>clients/">
                    <i class="material-icons">person</i>
                    <p>Clients</p>
                </a>
            </li>
             <li class="nav-item <?php echo ($industry_served)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>industry_served/">
                    <i class="material-icons">person</i>
                    <p>Industry Served</p>
                </a>
            </li>
             <li class="nav-item <?php echo ($download)?"active":"" ?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>download/">
                    <i class="material-icons">person</i>
                    <p>Downloads</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($settings)?"active":""?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>settings/">
                    <i class="material-icons">person</i>
                    <p>Settings</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($certificates)?"active":""?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>certificates/">
                    <i class="material-icons">person</i>
                    <p>Certificate</p>
                </a>
            </li>
            <li class="nav-item <?php echo ($events)?"active":""?>">
                <a class="nav-link" href="<?php echo ADMIN_BASE_URL ?>event/">
                    <i class="material-icons">person</i>
                    <p>Events</p>
                </a>
            </li>
            
            <!-- <li class="nav-item active-pro ">
                  <a class="nav-link" href="./upgrade.html">
                      <i class="material-icons">unarchive</i>
                      <p>Upgrade to PRO</p>
                  </a>
              </li> -->
        </ul>
    </div>
</div>