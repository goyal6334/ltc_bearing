<?php
namespace Admin\Pages;

class Pages{

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Pages Objects List
     */
    public function getPagesList(){
        global $mysqli;
        $query = "Select * from pages";
        if($result = $mysqli->query($query)){
            return $result->fetch_object();
        } else {
            return false;
        }
    }

}

$adminPagesClass = new Pages();