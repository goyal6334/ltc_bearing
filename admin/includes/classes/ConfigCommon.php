<?php
namespace Admin\ConfigCommon;

class ConfigCommon{

    public static $statusArray = array(
        1 => 'Active',
        0 => 'Inactive'
    );

    public function uiRedirect($url){
        echo "<script>window.location = '" . $url . "'</script>";
        exit();
    }

    public function setFlashSuccess($message){
        $_SESSION['flash_success'] = $message;
    }

    public function getFlashSuccess(){
        return $_SESSION['flash_success'];
    }

    public function setFlashError($message){
        $_SESSION['flash_error'] = $message;
    }

    public function getFlashError(){
        return $_SESSION['flash_error'];
    }

    public function resetFlash(){
        $_SESSION['flash_success'] = "";
        $_SESSION['flash_error'] = "";
    }
}

$configCommon = new ConfigCommon();