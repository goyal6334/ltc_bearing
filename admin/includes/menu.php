
<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top " id="navigation-example">
    <div class="container-fluid">
        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link" href="javscript:void(0)" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="material-icons">person</i>
                        <p class="d-lg-none d-md-block">
                            Account
                        </p>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
<!--                        <a class="dropdown-item" href="javascript:void(0)">Edit Profile</a>-->
                        <a class="dropdown-item" href="<?php echo ADMIN_BASE_URL . 'logout.php' ?>">Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
