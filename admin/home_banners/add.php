<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_POST)){
    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['title'])){
        $errorMsg = "Banner title is required.";
    } else {
        if($_FILES['image']['size'] < 1 || $_FILES['image']['error']){
            $errorMsg = "Invalid image to upload.";
        } else {
            $targetFilePath = ADMIN_BANNER_UPLOAD_DIR_PATH;
            if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if(empty($errorMsg)){
        $data = array(
            $adminBannersClass->title => $_POST['title'],
            $adminBannersClass->description => $_POST['description'],
            $adminBannersClass->image => $imagePath,
            $adminBannersClass->status => ($_POST['status'])?1:0,
            $adminBannersClass->type => $_POST['banner_type']
        );

        if($adminBannersClass->insert($data)){
            $configCommon->setFlashSuccess("Banner created successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'home_banners');
        } else {
            $errorMsg = "Unable to create banner, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Add Banner</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Banner Title</label>
                                    <input type="text" name="title" required="" class="form-control" value="<?php echo (!empty($_POST['title'])?$_POST['title']:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="banner_type" class="form-control" required="">
                                        <?php echo $configCommon->buildSelectOptions($adminBannersClass->getTypesDropdownList(), "Select Banner Type", ((!empty($_POST['banner_type'])) ? $_POST['banner_type'] : "")); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Banner Description</label>
                                    <textarea class="form-control" name="description" cols="10" rows="10"><?php echo (!empty($_POST['description'])?$_POST['description']:"") ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Banner Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($_POST['status'])?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Banner</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>