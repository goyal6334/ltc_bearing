<?php include_once('../includes/header.php') ?>
<?php
$homeBanners = $adminBannersClass->getList();
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="row">
                        <div class="col-sm-6">
                            <h4 class="card-title ">Banners</h4>
                        </div>
                        <div class="col-sm-6 text-right">
                            <a href="<?php echo ADMIN_BASE_URL . 'home_banners/add.php' ?>" class="btn btn-primary">Add Banner</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table" id="tblLocations">
                            <thead>
                                <th>
                                    ID
                                </th>
                                <th>
                                    Banner Title
                                </th>
                                <th>
                                    Banner Image
                                </th>
                                <th>
                                    Status
                                </th>
                                <th>
                                    Action
                                </th>
                            </thead>
                            <tbody>
                                <?php foreach($homeBanners as $homeBanner){ ?>
                                    <tr>
                                        <td>
                                            <?php echo $homeBanner[$adminBannersClass->id] ?>
                                        </td>
                                        <td>
                                            <?php echo @$homeBanner[$adminBannersClass->title] ?>
                                        </td>
                                        <td>
                                            <img style="width: 100px;" src="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $homeBanner[$adminBannersClass->image] ?>" alt="<?php echo $homeBanner[$adminBannersClass->title] ?>"/>
                                        </td>
                                        <td>
                                            <?php echo \Admin\ConfigCommon\ConfigCommon::$statusArray[$homeBanner[$adminBannersClass->status]] ?>
                                        </td>
                                        <td>
                                            <a href="<?php echo ADMIN_BASE_URL . 'home_banners/edit.php?id=' . $homeBanner[$adminBannersClass->id] ?>">Edit</a> /
                                            <a href="<?php echo ADMIN_BASE_URL . 'home_banners/delete.php?id=' . $homeBanner[$adminBannersClass->id] ?>">Delete</a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>