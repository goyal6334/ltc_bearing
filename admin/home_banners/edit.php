<?php include_once('../includes/header.php') ?>
<?php
if(!empty($_GET['id'])){
    $homeBanner = $adminBannersClass->getDetailsById($_GET['id']);
    if(empty($homeBanner)){
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'home_banners');
    }

    $title = $homeBanner[$adminBannersClass->title];
    $description = $homeBanner[$adminBannersClass->description];
    $status = $homeBanner[$adminBannersClass->status];
    $bannerType = $homeBanner[$adminBannersClass->type];
    $image = (!empty($homeBanner[$adminBannersClass->image]))?ADMIN_BANNER_UPLOAD_DIR_URL . $homeBanner[$adminBannersClass->image]:"";

} else {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'home_banners');
}
if(!empty($_POST)){
    $title = $_POST['title'];
    $description = $_POST['description'];
    $status = $_POST['status'];
    $bannerType = $_POST['banner_type'];

    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['title'])){
        $errorMsg = "Banner title is required.";
    } else {
        if($_FILES['image']['size'] > 0 && !$_FILES['image']['error']){
            $targetFilePath = ADMIN_BANNER_UPLOAD_DIR_PATH;
            if($imageName = $imageUploadClass->uploadImage($_FILES['image'], $targetFilePath)){

                //Delete old file
                if(!empty($homeBanner[$adminBannersClass->image])){
                    @unlink(ADMIN_BANNER_UPLOAD_DIR_PATH . $homeBanner[$adminBannersClass->image]);
                    rmdir(dirname(ADMIN_BANNER_UPLOAD_DIR_PATH . $homeBanner[$adminBannersClass->image]));
                }
                $imagePath = $imageName;
            } else {
                $errorMsg = "Invalid image to upload.";
            }
        }
    }

    if(empty($errorMsg)){
        $data[$adminBannersClass->title] = $_POST['title'];
        $data[$adminBannersClass->description] = $_POST['description'];
        if($imagePath){
            $data[$adminBannersClass->image] = $imagePath;
        }
        $data[$adminBannersClass->status] = ($_POST['status'])?1:0;
        $data[$adminBannersClass->type] = $_POST["banner_type"];

        if($adminBannersClass->update($_GET['id'], $data)){
            $configCommon->setFlashSuccess("Banner updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'home_banners');
        } else {
            $errorMsg = "Unable to update banner, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Banner</h4>
                </div>
                <div class="card-body">
                <?php if(!empty($errorMsg)){ ?>
                    <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Banner Title</label>
                                    <input type="text" name="title" class="form-control" required="" value="<?php echo (!empty($title)?$title:"") ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <select name="banner_type" class="form-control" required="">
                                        <?php echo $configCommon->buildSelectOptions($adminBannersClass->getTypesDropdownList(), "Select Banner Type", ((!empty($bannerType)) ? $bannerType : "")); ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Banner Description</label>
                                    <textarea class="form-control" name="description" cols="10" rows="10"><?php echo (!empty($description)?$description:"") ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Banner Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="file" class="form-control-file" name="image" style="opacity: 1;position: initial;">
                                                </div>
                                                <?php if(!empty($image)){ ?>
                                                    <div class="col-md-6">
                                                        <img style="width: 100px;" src="<?php echo $image ?>" alt="Banner Image"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="1" name="status" <?php echo (!empty($status)?"checked='checked'":"") ?>>
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                        Is Active
                                    </label>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary pull-right">Add Banner</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once('../includes/footer.php') ?>