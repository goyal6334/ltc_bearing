<?php include_once('../includes/config.php') ?>
<?php
if(!empty($_GET['id'])){
    $homeBanner = $adminBannersClass->getDetailsById($_GET['id']);
    if(empty($homeBanner)){
        $configCommon->setFlashError("Invalid banner id to delete.");
        $configCommon->uiRedirect(ADMIN_BASE_URL . 'home_banners');
    } else {
        if($adminBannersClass->delete($_GET['id'])){
            if(!empty($homeBanner[$adminBannersClass->image])){
                @unlink(ADMIN_BANNER_UPLOAD_DIR_PATH . $homeBanner[$adminBannersClass->image]);
                rmdir(dirname(ADMIN_BANNER_UPLOAD_DIR_PATH . $homeBanner[$adminBannersClass->image]));
            }
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'home_banners');
        } else {
            $configCommon->setFlashError("Unable to delete home banner, Please try again.");
        }
    }
} else {
    $configCommon->setFlashError("Invalid banner id to delete.");
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'home_banners');
}
?>
