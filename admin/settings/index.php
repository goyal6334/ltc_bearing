<?php
include_once('../includes/header.php');

$settings = $adminSettingsClass->getList();
if (empty($settings)) {
    $configCommon->uiRedirect(ADMIN_BASE_URL . 'settings');
}

$contactEmail = $settings[$adminSettingsClass->contactEmail];
$contactNumber = $settings[$adminSettingsClass->contactNumber];
$siteTitle = $settings[$adminSettingsClass->siteTitle];
$image = (!empty($settings[$adminSettingsClass->logo_img])) ? ADMIN_SETTINGS_UPLOAD_DIR_URL . $settings[$adminSettingsClass->logo_img] : "";

if (!empty($_POST)) {
    $errorMsg = "";
    $imagePath = false;
    if ($_FILES['logo']['size'] > 0 && !$_FILES['logo']['error']) {
        $targetFilePath = ADMIN_SETTINGS_DIR_PATH;
        if ($imageName = $imageUploadClass->uploadImage($_FILES['logo'], $targetFilePath)) {
            //Delete old file
            if (!empty($settings[$adminSettingsClass->logo_img])) {
                @unlink(ADMIN_SETTINGS_DIR_PATH . $settings[$adminSettingsClass->logo_img]);
                rmdir(dirname(ADMIN_SETTINGS_DIR_PATH . $settings[$adminSettingsClass->logo_img]));
            }
            $_POST['logo_img'] = $imageName;
        } else {
            $errorMsg = "Invalid image to upload.";
        }
    }

    if (empty($errorMsg)) {
        $allSettingsSaved = true;

        foreach ($settings as $settingKey => $settingDbValue) {
            if ($_POST[$settingKey] != $settingDbValue) {
                if (!$adminSettingsClass->setSetting($settingKey, $_POST[$settingKey])) {
                    $allSettingsSaved = false;
                }
            }
        }

        if ($allSettingsSaved) {
            $configCommon->setFlashSuccess("Settings updated successfuly.");
            $configCommon->uiRedirect(ADMIN_BASE_URL . 'settings');
        } else {
            $errorMsg = "Unable to update some settings, please try again.";
        }
    }
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title">Update Settings</h4>
                </div>
                <div class="card-body">
                    <?php if (!empty($errorMsg)) { ?>
                        <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                    <?php } ?>
                    <form action="" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Email Id</label>
                                    <input type="text" name="<?php echo $adminSettingsClass->contactEmail ?>" class="form-control" value="<?php echo (!empty($settings[$adminSettingsClass->contactEmail]) ? $settings[$adminSettingsClass->contactEmail] : "") ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Contact No.</label>
                                    <input type="text" name="<?php echo $adminSettingsClass->contactNumber; ?>" class="form-control"value="<?php echo (!empty($settings[$adminSettingsClass->contactNumber]) ? $settings[$adminSettingsClass->contactNumber] : "") ?>">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="bmd-label-floating">Site Title.</label>
                                    <input type="text" name="<?php echo $adminSettingsClass->siteTitle; ?>" class="form-control"value="<?php echo (!empty($settings[$adminSettingsClass->siteTitle]) ? $settings[$adminSettingsClass->siteTitle] : "") ?>">
                                </div>
                            </div>
                        </div>

                        <br />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label class="bmd-label-floating">Logo Image</label>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <input type="file" class="form-control-file" name="logo" style="opacity: 1;position: initial;">
                                                </div>
                                                <?php if (!empty($image)) { ?>
                                                    <div class="col-md-6">
                                                        <img style="width: 100px;" src="<?php echo $image ?>" alt="Logo Image"/>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>


                        <button type="submit" class="btn btn-primary pull-right">Update Settings</button>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
    function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
    return false;
    } else{
    return true;
    }
    });
</script>
<?php include_once('../includes/footer.php') ?>