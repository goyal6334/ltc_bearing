<?php include('common/header.php'); ?>
<div class="container-fluid">
        <div class="row row-fluid">
            <div class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("team") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>               
            </div>
        </div>
</div>

<section class="noo-page-heading">
    <div class="container">
        <h1 class="page-title">Sub Category Name</h1>
    </div>
</section>

<div class="main-content container shop-container commerce">
    <div class="row">
        <div class="noo-main col-md-8">
            <div class="products row">

                <div class="col-md-4 col-sm-6 col-xs-6 noo-product-item product">
                    <div class="noo-product-inner">
                        <div class="woo-thumbnail">
                            <div class="bk"></div>
                            <a href="#"><img src="images/cat.jpg" /></a>
                            <div class="noo-product-meta">
                                <div class="entry-cart-meta"><a href="#" class="button add_to_cart_button">Send Enquiry</a>
                                    <div class="yith-wcwl-add-to-wishlist">
                                        <div class="yith-wcwl-add-button"><a href="#" class="add_to_wishlist"> View Details</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="noo-product-footer"><h3><a href="">Product Name</a></h3></div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 noo-product-item product">
                    <div class="noo-product-inner">
                        <div class="woo-thumbnail">
                            <div class="bk"></div>
                            <a href="#"><img src="images/cat.jpg" /></a>
                            <div class="noo-product-meta">
                                <div class="entry-cart-meta"><a href="#" class="button add_to_cart_button">Send Enquiry</a>
                                    <div class="yith-wcwl-add-to-wishlist">
                                        <div class="yith-wcwl-add-button"><a href="#" class="add_to_wishlist"> View Details</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="noo-product-footer"><h3><a href="">Product Name</a></h3></div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 noo-product-item product">
                    <div class="noo-product-inner">
                        <div class="woo-thumbnail">
                            <div class="bk"></div>
                            <a href="#"><img src="images/cat.jpg" /></a>
                            <div class="noo-product-meta">
                                <div class="entry-cart-meta"><a href="#" class="button add_to_cart_button">Send Enquiry</a>
                                    <div class="yith-wcwl-add-to-wishlist">
                                        <div class="yith-wcwl-add-button"><a href="#" class="add_to_wishlist"> View Details</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="noo-product-footer"><h3><a href="">Product Name</a></h3></div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 noo-product-item product">
                    <div class="noo-product-inner">
                        <div class="woo-thumbnail">
                            <div class="bk"></div>
                            <a href="#"><img src="images/cat.jpg" /></a>
                            <div class="noo-product-meta">
                                <div class="entry-cart-meta"><a href="#" class="button add_to_cart_button">Send Enquiry</a>
                                    <div class="yith-wcwl-add-to-wishlist">
                                        <div class="yith-wcwl-add-button"><a href="#" class="add_to_wishlist"> View Details</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="noo-product-footer"><h3><a href="">Product Name</a></h3></div>
                    </div>
                </div>

                <div class="col-md-4 col-sm-6 col-xs-6 noo-product-item product">
                    <div class="noo-product-inner">
                        <div class="woo-thumbnail">
                            <div class="bk"></div>
                            <a href="#"><img src="images/cat.jpg" /></a>
                            <div class="noo-product-meta">
                                <div class="entry-cart-meta"><a href="#" class="button add_to_cart_button">Send Enquiry</a>
                                    <div class="yith-wcwl-add-to-wishlist">
                                        <div class="yith-wcwl-add-button"><a href="#" class="add_to_wishlist"> View Details</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="noo-product-footer"><h3><a href="">Product Name</a></h3></div>
                    </div>
                </div>

            </div>
        </div>

        <div class="noo-sidebar col-md-4">
            <div class="noo-sidebar-wrap">
                <div class="widget widget_categories">
                    <h4 class="widget-title">Our Products</h4>
                    <ul>
                        <li><a href="#">INA</a></li>
                        <li><a href="#">CPC</a></li>
                        <li><a href="#">ABBA</a></li>
                        <li><a href="#">BD</a></li>
                        <li><a href="#">IKO</a></li>
                        <li><a href="#">LM SERIES</a></li>
                        <li><a href="#">KGB Plain Bearings</a></li>
                        <li><a href="#">Lubricating Bushes and Bearing</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>

<?php include('common/footer.php'); ?>