<?php include('common/header.php'); ?>
<?php
if(!empty($_POST)){
    $errorMsg = "";
    $imagePath = false;
    if(empty($_POST['name'])){
        $errorMsg = "Your name is required.";
    } else {
        if(empty($_POST['email'])){
            $errorMsg = "Email address is required.";
        } else {
            if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
                $errorMsg = "Invalid email address.";
            } else {
                if(empty($_POST['sub'])){
                    $errorMsg = "Subject is required.";
                } else {
                    if(empty($_POST['description'])){
                        $errorMsg = "Message is required.";
                    }
                }
            }
        }
    }
    if(empty($errorMsg)){
        $content = "Name: " . $_POST['name'] . "<br>";
        $content .= "Email: " . $_POST['email'] . "<br>";
        $content .= "Phone: " . $_POST['mobile'] . "<br>";
        $content .= "Message: " . $_POST['description'];

        if($emailClass->sendSmtpEmail(Admin\ConfigCommon\ConfigCommon::$adminEmail, Admin\ConfigCommon\ConfigCommon::$adminName, $_POST['sub'], $content)){
            $successMsg = "Email sent successfully, We will contact you soon.";
        } else {
            $errorMsg = "Something went wrong, Please try after some time.";
        }
    }
}
?>
<div class="container-fluid">
        <div class="row row-fluid">
            <div data-aos="fade-right" class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("team") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>               
            </div>
        </div>
    </div>

<div class="main-content">

    <div data-aos="fade-down" class="container-fluid">
        <div class="row row-fluid">
            <div class="col-sm-12">
                <div class="google-map">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3501.1596611541604!2d77.21705831440775!3d28.65493768981881!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390cfd131e668c2d%3A0xeabd18a52664c07a!2sLakshmi%20Trading%20Company%2CINA%20Bearing%2C%20Needle%20Bearing%2C%20Tsubaki%20Cam%20Clutch%20Suppliers!5e0!3m2!1sen!2sin!4v1595922077128!5m2!1sen!2sin" width="100%" height="400" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row row-fluid">
            <div class="col-sm-12">
                <div class="noo-customform book-creative" data-aos="fade-left">
                    <div class="container">
                        <div class="book-form2">
                            <h2><strong>SEND ENQUIRY</strong></h2>
                            <?php if(!empty($errorMsg)){ ?>
                                <?php echo "<div class='alert alert-danger'>" . $errorMsg . "</div>" ?>
                            <?php } ?>
                            <?php if(!empty($successMsg)){ ?>
                                <?php echo "<div class='alert alert-success'>" . $successMsg . "</div>" ?>
                            <?php } ?>
                            <div class="book-creative-content">
                                <form method="post" data-aos="fade-right">
                                    <div class="noo-form">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="wpcf7-form-control-wrap">
                                                    <input type="text" name="name" value="" size="40" class="wpcf7-form-control" placeholder="Name" />
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="wpcf7-form-control-wrap carle-name">
                                                    <input type="email" name="email" value="" size="40" class="wpcf7-form-control" placeholder="E-mail" />
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="wpcf7-form-control-wrap carle-email">
                                                    <input type="text" name="mobile" value="" size="40" class="wpcf7-form-control" placeholder="Mobile" />
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="wpcf7-form-control-wrap carle-email">
                                                    <input type="text" name="sub" value="" size="40" class="wpcf7-form-control" placeholder="Subject" />
                                                </div>
                                            </div>

                                            <div class="col-md-12">
                                                <div class="wpcf7-form-control-wrap description">
                                                    <textarea name="description" cols="40" rows="10" class="wpcf7-form-control" placeholder="Describe your issue"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="book-form-submit">
                                            <input type="submit" value="send mail" class="wpcf7-form-control wpcf7-submit" />
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="svg-top">
                                <svg viewBox="0 0 100 100" preserveAspectRatio="none">
                                <path d="M0 0 L100 0 L0 100" stroke-width="0"></path>
                                </svg>
                            </div>
                            <div class="svg-bottom">
                                <svg viewBox="0 0 100 100" preserveAspectRatio="none">
                                <path d="M0 0 L100 100 L0 100" stroke-width="0"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div data-aos="fade-up" class="container-fluid">
        <div class="row row-fluid pt-3">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h4 class="text-center"> Are you looking for a best service? We want to hear from you! </h4>
                        <p class="text-center">Lakshmi Trading Company offers in house brand MNB LTC sources its product range from top producers of bearings worldwide ensures 
                            uncompromising quality as per ISO standards and supplies them to customers at competitive prices. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div data-aos="fade-down" class="container-fluid">
        <div class="row row-fluid pt-3 pb-11">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-4 pt-3">
                        <div class="noo-information infor-left"> <i class="fa fa-phone"></i> <strong>Online support 24/7</strong> <b><?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactNumber); ?></b> </div>
                    </div>
                    <div class="col-sm-12 col-md-4 pt-3">
                        <div class="noo-information infor-left"> <i class="fa fa-envelope"></i> <strong>Mail Us</strong> <b><?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactEmail); ?></b> </div>
                    </div>
                    <div class="col-sm-12 col-md-4 pt-3">
                        <div class="noo-information infor-left"> <i class="fa fa-map-marker"></i> <strong>Address</strong> <b>1519/3,GALI , AKHUNJI FRASH KHANA G.B.ROAD , DELHI,110006</b> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('common/footer.php'); ?>