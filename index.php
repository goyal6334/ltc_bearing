<?php include('common/header.php'); ?>
<div class="main-content">
<div class="container-fluid">
<div class="row row-fluid">
<div data-aos="fade-right" class="col-sm-12">
<div class="rev_slider_wrapper fullwidthbanner-container">
<div id="rev_slider" class="rev_slider fullwidthabanner">
<ul>
                            <?php foreach($adminBannersClass->getImg(BANNER_TYPE_HOME) as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
</li>
                            <?php } ?>
</ul>
</div>
</div>
<!-- END REVOLUTION SLIDER -->
</div>
</div>
</div>

<div class="container-fluid animate__animated animate__backInDown ">
<div class="row row-fluid ">
<div class="container">
<div class="row">
<div class="col-sm-12 col-md-12">
<div class="whychooseus choose_style2">
<div class="choose-content">
<div class="title-center">
    <h2 class="intro-title-1 center-block" data-aos="flip-left">LTC Bearing</h2>
    
</div>
<p data-aos="fade-up">Lakshmi Trading Company was established to be a one stop solution, offering world class bearings through partnering with leading manufacturers across the Globe.
<br><br>
Since its inception in 1994, Our Founder Mr. R.B Maurya set upon his dream in the form of Lakshmi Trading & he showed the seeds of leadership, hard-work & commitment.
<br><br>
Lakshmi Trading Company also known as (LTC Bearing), Supplier of world renowned bearing brands INA-FAG- Germany, Tsubaki Japan, NSK-RHP- Japan, THK-Japan, MCGILL-USA, IKO-Japan, NTN-Japan, OZAK, Japan, EZO, JAF, ABBA Linear Bearing </p>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<br><br>

<section class="section-area wow" data-aos="fade-down">
<ul class="b-advantages-4">
<li class="b-advantages-4__item">
<div class="b-advantages-4__inner">
<div class="b-advantages-4__title">Company Profile</div>
<i class="b-advantages-4__decor pe-7s-angle-down"></i>
<div class="b-advantages-4__text">
    Lakshmi Trading Company has become the Largest & Best-known Supplier 
    of Bearings in India. Lakshmi Trading Company has grown rapidly in almost in 
    every segment they operates viz. Machine Tool, Switchgear, Motor & Gearbox, 
    Pharma Machinery, Steel Mill, Laser Machinery, Textile, Power Plant, 
    General Industry, Defence, many more........
</div>
</div>
</li>
<li class="b-advantages-4__item">
<div class="b-advantages-4__inner">
<div class="b-advantages-4__title">Director's Message</div>
<i class="b-advantages-4__decor pe-7s-angle-down"></i>
<div class="b-advantages-4__text">
    Lakshmi Trading Company established by Mr. R.B Maurya in the year 1994 is one of the largest importer & 
    stockist of bearings. Situated in the business capital of India.
    Delhi it carries very good name & goodwill in the market.
    We keep bearings in wide range useful for various industries & market.
</div>
</div>
</li>
<li class="b-advantages-4__item">
<div class="b-advantages-4__inner">
<div class="b-advantages-4__title">Why Us</div>
<i class="b-advantages-4__decor pe-7s-angle-down"></i>
<div class="b-advantages-4__text">
    We endeavor to reach the leadership position in each sector and Service. 
    We are committed to satisfy our customers by providing Quality Product, Service, which gives highest value for money. 
    We commit ourselves to continuous growth, to fulfill the aspirations of our Customer.
</div>
</div>
</li>
<li class="b-advantages-4__item">
<div class="b-advantages-4__inner">
<div class="b-advantages-4__title">Quality Policy</div>
<i class="b-advantages-4__decor pe-7s-angle-down"></i>
<div class="b-advantages-4__text">
    At Lakshmi Trading Company, we are committed to building value for our customers, our business, 
    as well as our employees. We strive to conduct our business affairs with the highest ethical 
    standards and work diligently to be a respected corporate citizen worldwide.


</div>
</div>
</li>
</ul>
</section>

<div class="container-fluid">
<div class="row row-fluid">
<div class="col-sm-12">
<div class="noo-single-product single-bg-1">
<div class="container noo-sh-product">
<div class="row">
<div class="col-md-6 ">
<h3 class="intro-title-1" data-aos="flip-left">Our Believes</h3>
<p data-aos="flip-right">Lakshmi Trading Company believes in quality products and services are the 
    benchmark. Each and every customer is treated as an opportunity and 
    every achievement as a platform to set new goals. This strategy has 
    enabled Lakshmi Trading Company to have delighted customers in India. 
    </p>
</div>
<div class="col-md-5 col-md-offset-1 col-sm-offset-0">
<div class="product-right"><img width="553" height="382" src="images/image1.png" data-aos="fade-right" class="single-product-img"/></div>
</div>
</div>
</div>
<div class="svg-bottom">
<svg viewBox="0 0 100 100" preserveAspectRatio="none">
<path d="M100 0 L100 100 L0 100" stroke-width="0"></path>
</svg>
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid">
<div class="row row-fluid">
<div class="col-sm-12">
<div class="noo-single-product single-bg-2">
<div class="container noo-sh-product noo-sh-product2">
<div class="row">
<div class="col-md-6">
<div class="product-right"><img width="656" height="518" data-aos="fade-left" src="images/image1.png"/></div>
</div>
<div class="col-md-5 col-md-offset-1 col-sm-offset-0">
<h3 class="intro-title-1" data-aos="flip-left">Our Motto</h3>
<p data-aos="flip-right">We are a progressive and innovative company. Our Three fold business 
    motto is Total dedication, Total commitment and Total Quality. 
    Combining it with our passion for applying our learning from 
    business to further enhance our performance we are confident of setting even 
    higher benchmarks for the Indian Bearing Industry in the years to come.</p>
</div>
</div>
</div>
<div class="svg-bottom line-out">
<svg viewBox="0 0 100 100" preserveAspectRatio="none">
<path d="M0 0 L100 0 L100 100" stroke-width="0"></path>
</svg>
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid">
<div class="row row-fluid">
<div class="col-sm-12">
<div class="noo-single-product single-bg-3">
<div class="container noo-sh-product white-gold">
<div class="row">
<div class="col-md-6 product-left-content">
    <h3 style="color:yellow;" data-aos="flip-left">Our Goodwill</h3>
<p data-aos="flip-right">
    Lakshmi Trading Company is committed to delivering technology 
    that provides solutions to varied needs of Indian Industry. 
    We are honored to have been recognized by Indian Industry 
    as the most reliable source for bearings in India.
    We are indebted to the society to which we belong 
    and are committed to contribute to its betterment and well-being.

</p>
</div>
<div class="col-md-5 col-md-offset-1 col-sm-offset-0">
<div class="product-right"><img width="655" height="482" src="images/image1.png" data-aos="fade-right" class="single-product-img" /></div>
</div>
</div>
</div>
<div class="svg-bottom">
<svg viewBox="0 0 100 100" preserveAspectRatio="none">
<path d="M100 0 L100 100 L0 100" stroke-width="0"></path>
</svg>
</div>
</div>
</div>
</div>
</div>


<div class="container-fluid">
<div class="row row-fluid pt-12">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="title-center">
<h2 class="intro-title-1" data-aos="flip-up"><strong>Our Products</strong> </h2>
</div>
</div>
</div>
</div>
</div>
</div>

<div class="container-fluid">
<div class="row row-fluid">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="tabbable product-tab">
<!--<ul class="nav nav-tabs">
<li class="active"> <a data-toggle="tab" href="#tab-1">New Products</a> </li>
<li> <a data-toggle="tab" href="#tab-2">Popular Products</a> </li>
<li> <a data-toggle="tab" href="#tab-3">Best Selling</a> </li>
<li> <a data-toggle="tab" href="#tab-4">Sale Products</a> </li>
</ul>-->
<br><br>
<div class="tab-content">

<div id="tab-1" class="tab-pane fade in active">
<div class="noo-row-slider commerce">
<ul class="noo-woo-slider products" data-aos="flip-down">
<li>
<div class="col-md-4 col-sm-6 col-xs-6 product">
<div class="noo-product-inner">
<div class="woo-thumbnail">
<div class="bk"></div>
<img width="270" height="350" src="images/cat.jpg"/>
<div class="noo-product-meta">
<div class="entry-cart-meta"><a href="#" class="add_to_cart_button">View Products</a></div>
</div>
</div>
<div class="noo-product-footer"><h3><a href="#">Category Name</a></h3></div>
</div>
</div>
</li>
<li>
<div class="col-md-4 col-sm-6 col-xs-6 product">
<div class="noo-product-inner">
<div class="woo-thumbnail">
<div class="bk"></div>
<img width="270" height="350" src="images/cat.jpg"/>
<div class="noo-product-meta">
<div class="entry-cart-meta"><a href="#" class="add_to_cart_button">View Products</a></div>
</div>
</div>
<div class="noo-product-footer"><h3><a href="#">Category Name</a></h3></div>
</div>
</div>
</li>
<li>
<div class="col-md-4 col-sm-6 col-xs-6 product">
<div class="noo-product-inner">
<div class="woo-thumbnail">
<div class="bk"></div>
<img width="270" height="350" src="images/cat.jpg"/>
<div class="noo-product-meta">
<div class="entry-cart-meta"><a href="#" class="add_to_cart_button">View Products</a></div>
</div>
</div>
<div class="noo-product-footer"><h3><a href="#">Category Name</a></h3></div>
</div>
</div>
</li>
<li>
<div class="col-md-4 col-sm-6 col-xs-6 product">
<div class="noo-product-inner">
<div class="woo-thumbnail">
<div class="bk"></div>
<img width="270" height="350" src="images/cat.jpg"/>
<div class="noo-product-meta">
<div class="entry-cart-meta"><a href="#" class="add_to_cart_button">View Products</a></div>
</div>
</div>
<div class="noo-product-footer"><h3><a href="#">Category Name</a></h3></div>
</div>
</div>
</li>
<li>
<div class="col-md-4 col-sm-6 col-xs-6 product">
<div class="noo-product-inner">
<div class="woo-thumbnail">
<div class="bk"></div>
<img width="270" height="350" src="images/cat.jpg"/>
<div class="noo-product-meta">
<div class="entry-cart-meta"><a href="#" class="add_to_cart_button">View Products</a></div>
</div>
</div>
<div class="noo-product-footer"><h3><a href="#">Category Name</a></h3></div>
</div>
</div>
</li>
</ul>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>


<div class="container-fluid">
<div class="row row-fluid noo-clients-default-3 noo-client-bg">
<div class="col-sm-12 col-md-6 testimonial-bg" data-aos="fade-up-right">
<ul class="noo_testimonial noo_testimonial_one">
<li> <i class="fa fa-quote-left moo-quote-icon"></i>
<div class="testimonial-content">
    <p data-aos="fade-up-left" style="color:yellow !important;">We endeavor to reach the leadership position 
    in each sector and Service. We are committed 
    to satisfy our customers by providing Quality 
    Product, Service, 
    which gives highest value for money. 
    We commit ourselves to continuous growth, 
    to fulfill the aspirations of our Customer.
    </p>
</div>
<div class="testimonial-ds"><h4 class="noo_testimonial_name"></h4></div>
</li>

</ul>
</div>
<div class="col-sm-12 col-md-6 noo-clients">
<div class="row row-fluid">
<div class="col-sm-8">
<div class="title-left">
<h3 class="intro-title-1" data-aos="flip-right">Our Clients</h3>
</div>
</div>

<div class="col-sm-4"></div>
</div>
<div class="clients noo-clients-default">
<div class="row">
                        
<ul class="noo-slider-clients " data-aos="flip-left">
                            <?php foreach ($adminClientsClass->getImg() as $image){?>
                            <li class="noo_client_item"><a><img width="145" height="125" src='<?php echo ADMIN_CLIENT_UPLOAD_DIR_URL . $image['image'];?> ' /></a></li>
                            <?php }?>
</ul>
</div>
</div>
</div>
</div>
</div>

</div>

<?php include('common/footer.php'); ?>
