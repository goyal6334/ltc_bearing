<?php
namespace Admin\Login;

use Admin\User\User;

class Login{
    
    private $loginKey = "ADMIN_LOGIN";

    /**
     * 
     * @param type $email
     * @return boolean
     * 
     * Set login user in session
     */
    public function setLoginUser($userData){
        $_SESSION[$this->loginKey] = $userData;
        return true;
    }

    /**
     * 
     * @return boolean
     * 
     * Check user is login or not
     */
    public function hasLogin(){
        if(!empty($_SESSION[$this->loginKey])){
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * @return boolean
     * 
     * reset backend login session
     */
    public function resetLogin(){
        $_SESSION[$this->loginKey] = "";
        return true;
    }
}

$adminLoginClass = new Login();