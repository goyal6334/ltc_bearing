<?php

namespace Admin\Category;

use Admin\ConfigCommon\ConfigCommon;

class Category {

    public $table_name = "categories";
    public $id = "id";
    public $parent_id = "parent_id";
    public $name = "name";
    

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Gallery Objects List
     */
    public function getList() {
        global $mysqli;

        $query = "SELECT * FROM $this->table_name";
        if ($result = $mysqli->query($query)) {
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * 
     * @return type
     * 
     * Get category array for Hierarchy
     */
    public function getCategoriesHierarchy(){
        $categoriesList = $this->getList();
        $configCommonObj = new ConfigCommon();
        $list = $configCommonObj->getTreeList($categoriesList);
        
        return $list;
    }

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Gallery Objects Dropdown List
     */
    public function getDropdownList($dbSelectedId = false) {
        global $mysqli;

        $condition = "";
        if ($dbSelectedId) {
            $childCats = $this->fetchChildren($this->getAndUpdateCachedCategoryArray(true), $dbSelectedId);
            $condition = " WHERE id NOT IN (" . implode(",", $childCats) . ")";
        }
        $query = "SELECT * FROM $this->table_name" . $condition;
        if ($result = $mysqli->query($query)) {
            $dropdownList = array();
            foreach ($result->fetch_all(MYSQLI_ASSOC) as $rowData) {
                $dropdownList[$rowData[$this->id]] = $this->getCategoryName($rowData[$this->id]);
            }
            return $dropdownList;
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\certificates\type $mysqli
     * @param type $certificates
     * @return boolean
     * 
     * Insert banner data in DB
     */
    public function insert($categoryData) {
        global $mysqli;

        $columns = implode(',', array_keys($categoryData));
        $values = '"' . implode('","', array_values($categoryData)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if ($result = $mysqli->query($query)) {
            if ($mysqli->affected_rows > 0) {
                $this->getAndUpdateCachedCategoryArray(true);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Certificates\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update gallery record in DB
     */
    public function update($recordId, $categoryData) {
        global $mysqli;

        $count = 1;
        foreach ($categoryData as $columnName => $columnValue) {
            if (count($categoryData) > $count) {
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if ($result = $mysqli->query($query)) {
            if ($mysqli->affected_rows > 0) {
                $this->getAndUpdateCachedCategoryArray(true);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDetailsById($id) {
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if ($result = $mysqli->query($query)) {
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id) {
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if ($result = $mysqli->query($query)) {
            if ($mysqli->affected_rows > 0) {
                $this->getAndUpdateCachedCategoryArray(true);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @param type $forceUpdate
     * @return type
     * 
     * Get And Update Cached categories array
     */
    public function getAndUpdateCachedCategoryArray($forceUpdate = false) {
        if (empty($_SESSION["ltc_cached_category_array"]) || $forceUpdate) {
            $categories = $this->getList();
            $categoryArray = array();
            foreach ($categories as $category) {
                $categoryArray[$category[$this->id]] = array($this->parent_id => $category[$this->parent_id], $this->name => $category[$this->name]);
            }
            $_SESSION['ltc_cached_category_array'] = $categoryArray;
        }

        return $_SESSION["ltc_cached_category_array"];
    }

    /**
     * 
     * @param type $id
     */
    public function getCategoryName($id, $recorsiveCategoryName = "") {
        $categoriesArray = $this->getAndUpdateCachedCategoryArray();
        
        if ($categoriesArray[$id][$this->parent_id] > 0) {

            $categoryName = "/" . $categoriesArray[$id][$this->name];
            if (!empty($recorsiveCategoryName)) {
                $categoryName .= $recorsiveCategoryName;
            } else {
                $categoryName .= "";
            }

            return $this->getCategoryName($categoriesArray[$id][$this->parent_id], $categoryName);
        } else {

            if (!empty($recorsiveCategoryName)) {
                $categoryName .= $recorsiveCategoryName;
            } else {
                $categoryName .= "";
            }
            
            return $categoriesArray[$id][$this->name] . $categoryName;
        }
    }

    public function fetchChildren($childArr, $parentId, $parentfound = false, $cats = array()) {
        foreach ($childArr as $rowId => $row) {
            if ((!$parentfound && $rowId == $parentId) || $row[$this->parent_id] == $parentId) {
                $rowdata = array();
                foreach ($row as $k => $v) {
                    $cats[$rowId] = $rowId;
                }

                if ($row[$this->parent_id] == $parentId) {
                    $cats = array_merge($cats, $this->fetchChildren($childArr, $rowId, true));
                }
            }
        }
        return $cats;
    }

}

$categoryClass = new Category();
