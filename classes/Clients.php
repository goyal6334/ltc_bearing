<?php
namespace Admin\Clients;

class Clients{

    public $table_name = "clients";

    public $id = "id";
    public $name = "name";    
    public $image = "image";
    public $status = "status";
    

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Pages Objects List
     */
    public function getList(){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name ";
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /*
 * Developed by : Shivani
 * Date : 06-10-2020
 */
public function getImg(){
    
    global $mysqli;
    $query = "SELECT $this->image,$this->name FROM $this->table_name where status=1";
    
    if ($result=$mysqli->query($query))
    {
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    else
    {
        return FALSE;
    }
}
    
    /**
     * 
     * @global \Admin\Clients\type $mysqli
     * @param type $data
     * @return boolean
     * 
     * Insert client data in DB
     */
    public function insert($data){
        global $mysqli;

        $columns = implode(',', array_keys($data));
        $values = '"' . implode('","', array_values($data)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Clients\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update banner record in DB
     */
    public function update($recordId, $clientsData){
        global $mysqli;

        $count = 1;
        foreach($clientsData as $columnName => $columnValue){
            if(count($clientsData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

     public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminClientsClass = new Clients();