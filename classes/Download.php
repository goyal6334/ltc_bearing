<?php
namespace Admin\Download;

class Download{

    public $table_name = "download";

    public $id = "id";
    public $title = "title";    
    public $image = "image";
    public $status = "status";
    public $startdate = "startdate";
    public $enddate = "enddate";
    public $Url = "url";
    

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Pages Objects List
     */
    public function getList(){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name ";
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Clients\type $mysqli
     * @param type $data
     * @return boolean
     * 
     * Insert client data in DB
     */
    public function insert($data){
        global $mysqli;

        $columns = implode(',', array_keys($data));
        $values = '"' . implode('","', array_values($data)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\download\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update banner record in DB
     */
    public function update($recordId, $downloadData){
        global $mysqli;
        $columnAndValues = "";
        $count = 1;
        foreach($downloadData as $columnName => $columnValue){
            if(count($downloadData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return 2;
            }
        } else {
            return false;
        }
    }

     public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminDownloadsClass = new Download();