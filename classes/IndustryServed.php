<?php
namespace Admin\industry_served;

class IndustryServed{

    public $table_name = "industry_served";
    public $id = "id";
    public $title = "title";
    public $image = "image";
    public $status = "status";

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Gallery Objects List
     */
    public function getList(){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name";
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }
/*
 * Developed by : Shivani
 * Date : 06-10-2020
 */
public function getImg(){
    
    global $mysqli;
    $query = "Select $this->image,$this->title from $this->table_name where status=1";
    if ($result=$mysqli->query($query))
    {
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    else
    {
        return FALSE;
    }
}

/**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $bannerData
     * @return boolean
     * 
     * Insert banner data in DB
     */
    public function insert($industry_servedData){
        global $mysqli;

        $columns = implode(',', array_keys($industry_servedData));
        $values = '"' . implode('","', array_values($industry_servedData)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update gallery record in DB
     */
    public function update($recordId, $industry_servedData){
        global $mysqli;

        $count = 1;
        foreach($industry_servedData as $columnName => $columnValue){
            if(count($industry_servedData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminIndustryClass = new IndustryServed();