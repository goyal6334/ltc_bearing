<?php
namespace Admin\Gallery;

class Gallery{

    public $table_name = "gallery";
    public $id = "id";
    public $title = "title";
    public $image = "image";
    public $status = "status";

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Gallery Objects List
     */
    public function getList(){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name";
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $bannerData
     * @return boolean
     * 
     * Insert banner data in DB
     */
    public function insert($galleryData){
        global $mysqli;

        $columns = implode(',', array_keys($galleryData));
        $values = '"' . implode('","', array_values($galleryData)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update gallery record in DB
     */
    public function update($recordId, $bannerData){
        global $mysqli;

        $count = 1;
        foreach($bannerData as $columnName => $columnValue){
            if(count($bannerData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminGalleryClass = new Gallery();