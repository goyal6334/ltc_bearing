<?php
namespace Admin\Product;

class Product{

    public $table_name = "product";
    public $id = "id";
    public $title = "title";
    public $category_id = "category_id";
    public $content = "content";
    public $image = "image";
    public $status = "status";

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Gallery Objects List
     */
    public function getList($categoryId = ""){
        global $mysqli;

        if(!empty($categoryId)){
            $condition = " WHERE category_id='" . $categoryId . "'";
        }
        $query = "SELECT * FROM $this->table_name" . $condition;
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Product\type $mysqli
     * @param type $ProductData
     * @return boolean
     * 
     * Insert Product data in DB
     */
    public function insert($ProductData){
        global $mysqli;
        
        $columns = implode(',', array_keys($ProductData));
        $values = '"' . implode('","', array_values($ProductData)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $recordId
     * @param type $ProductData
     * @return boolean
     * 
     * Update gallery record in DB
     */
    public function update($recordId, $ProductData){
        global $mysqli;

        $count = 1;
        foreach($ProductData as $columnName => $columnValue){
            if(count($ProductData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminProductClass = new Product();