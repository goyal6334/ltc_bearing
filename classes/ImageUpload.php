<?php
namespace Admin\ImageUpload;

class ImageUpload{

    /**
     * 
     * @param type $source of $_FILE
     * @param type $destination
     * @param type default $extAllowed array('jpg', 'png', 'jpeg', 'gif')
     * 
     * 
     * Upload image from $_FILE
     */
    public function uploadImage($source, $destinationDir, $extAllowed = array('jpg', 'png', 'jpeg', 'gif')){
        $imageFileExtention = strtolower(pathinfo($source['name'], PATHINFO_EXTENSION));
        if(in_array($imageFileExtention, $extAllowed)){
            $fileDirName = md5(rand());
            if(!file_exists($destinationDir . $fileDirName)){
                mkdir($destinationDir . $fileDirName, 0777, true);
            }

            $fileName =  $fileDirName . "/" . md5(rand()) . '.' . $imageFileExtention;
            if (move_uploaded_file($source["tmp_name"], $destinationDir . $fileName)) {
                return $fileName;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}

$imageUploadClass = new ImageUpload();