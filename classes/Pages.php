<?php
namespace Admin\Pages;

class Pages{

    public $table_name = "pages";
    public $id = "id";
    public $title = "title";
    public $type = "type";
    public $content = "content";
    public $status = "status";

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Page Objects List
     */
    public function getList(){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name";
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Page\type $mysqli
     * @param type $PageData
     * @return boolean
     * 
     * Insert Page data in DB
     */
    public function insert($PageData){
        global $mysqli;
        
        $columns = implode(',', array_keys($PageData));
        $values = '"' . implode('","', array_values($PageData)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Page\type $mysqli
     * @param type $recordId
     * @param type $PageData
     * @return boolean
     * 
     * Update page record in DB
     */
    public function update($recordId, $ProductData){
        global $mysqli;

        $count = 1;
        foreach($ProductData as $columnName => $columnValue){
            if(count($ProductData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminPageClass = new Pages();