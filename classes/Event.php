<?php
namespace Admin\Event;

class Event{

    public $table_name = "event";

    public $id = "id";
    public $title = "title";    
    public $image = "image";
    public $status = "status";
    public $startdate = "startdate";
    public $enddate = "enddate";
    
    

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Pages Objects List
     */
    public function getList(){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name ";
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /*
     * Developed by : shivani
     * Date : 07-10-2020
     */
    
    public function getEvent($data){
        global $mysqli;
        
        $Todaydate = date("Y-m-d");
        
        if($data == 'past')
        {
            
            $query = "SELECT $this->image,$this->startdate,$this->enddate,$this->title FROM $this->table_name WHERE enddate<'$Todaydate' ORDER BY $this->enddate DESC LIMIT 4";
        }
        else
        {
             $query = "SELECT $this->image,$this->startdate,$this->enddate,$this->title FROM $this->table_name WHERE startdate>'$Todaydate' ORDER BY $this->startdate ASC";
        }
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

        /**
     * 
     * @global \Admin\Clients\type $mysqli
     * @param type $data
     * @return boolean
     * 
     * Insert client data in DB
     */
    public function insert($data){
        global $mysqli;

        $columns = implode(',', array_keys($data));
        $values = '"' . implode('","', array_values($data)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\download\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update banner record in DB
     */
    public function update($recordId, $eventData){
        global $mysqli;
        $columnAndValues = "";
        $count = 1;
        foreach($eventData as $columnName => $columnValue){
            if(count($eventData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return 2;
            }
        } else {
            return false;
        }
    }

     public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminEventsClass = new Event();