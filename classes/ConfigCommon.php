<?php

namespace Admin\ConfigCommon;

class ConfigCommon {

    public static $adminEmail = "goyal6335@gmail.com";

    public static $adminName = "Rohit Goyal";

    public static $statusArray = array(
        1 => 'Active',
        0 => 'Inactive'
    );

    public static $pageTypeArray = array(
        'page`' => 'Page',
        'content' => 'Content',
    );

    public function getPageType(){
        return self::$pageTypeArray;
    }

    public function uiRedirect($url) {
        echo "<script>window.location = '" . $url . "'</script>";
        exit();
    }

    public function setFlashSuccess($message) {
        $_SESSION['flash_success'] = $message;
    }

    public function getFlashSuccess() {
        if (!empty($_SESSION['flash_success'])) {
            return $_SESSION['flash_success'];
        } else {
            return "";
        }
    }

    public function setFlashError($message) {
        $_SESSION['flash_error'] = $message;
    }

    public function getFlashError() {
        if (!empty($_SESSION['flash_error'])) {
            return $_SESSION['flash_error'];
        } else {
            return "";
        }
    }

    public function resetFlash() {
        $_SESSION['flash_success'] = "";
        $_SESSION['flash_error'] = "";
    }

    public function buildSelectOptions($listData, $emptyText = false, $selectedValue = false) {
        $optionHtml = "";
        if ($emptyText) {
            $optionHtml .= "<option value=''>" . $emptyText . "</option>";
        }

        if (!empty($listData)) {
            foreach ($listData as $listKey => $listValue) {
                if($selectedValue == $listKey){
                    $optionHtml .= "<option value='" . $listKey . "' selected='selected'>" . $listValue . "</option>";
                } else {
                    $optionHtml .= "<option value='" . $listKey . "'>" . $listValue . "</option>";
                }
            }
        }
        return $optionHtml;
    }

    public function getTreeList($arrayData){
        $refs = array();
        $list = array();
        foreach ($arrayData as $row)
        {
            $ref = & $refs[$row['id']];

            $ref['parent_id'] = $row['parent_id'];
            $ref['name'] = $row['name'];
            $ref['related_product_id'] = $row['related_product_id'];
            $ref['id'] = $row['id'];

            if ($row['parent_id'] == 0)
            {
                $list[$row['id']] = & $ref;
            }
            else
            {
                $refs[$row['parent_id']]['children'][$row['id']] = & $ref;
            }
        }
        return $list;
    }
}

$configCommon = new ConfigCommon();
