<?php
namespace Admin\Certificates;

class Certificates{

    public $table_name = "certificates";
    public $id = "id";
    public $name = "name";
    public $image = "image";
    public $status = "status";

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Gallery Objects List
     */
    public function getList(){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name";
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

     /*
 * Developed by : Shivani
 * Date : 06-10-2020
 */
public function getImg(){
    
    global $mysqli;
    $query = "SELECT $this->image FROM $this->table_name where status=1";
    
    if ($result=$mysqli->query($query))
    {
        return $result->fetch_all(MYSQLI_ASSOC);
    }
    else
    {
        return FALSE;
    }
}
    
    /**
     * 
     * @global \Admin\certificates\type $mysqli
     * @param type $certificates
     * @return boolean
     * 
     * Insert banner data in DB
     */
    public function insert($certificatesData){
        global $mysqli;

        $columns = implode(',', array_keys($certificatesData));
        $values = '"' . implode('","', array_values($certificatesData)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Certificates\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update gallery record in DB
     */
    public function update($recordId, $certificatesData){
        global $mysqli;

        $count = 1;
        foreach($certificatesData as $columnName => $columnValue){
            if(count($certificatesData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}

$adminCertificatesClass = new Certificates();