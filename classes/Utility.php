<?php
namespace Admin\Utility;

use Admin\Category\Category;
use Admin\Product\Product;

class Utility{

    public $menuList;
    public $productClass;

    public function __construct() {
        $this->productClass = new Product();

    }
    public function convertDateIntoNewFormat($date, $oldFormat, $newFormat){
        return \DateTime::createFromFormat($oldFormat, $date)->format($newFormat);
    }

    public function getMenus(){
        $categoryClass = new Category();
        $categories = $categoryClass->getCategoriesHierarchy();

        $newArray = $this->setProductInCategories($categories);
        return $this->createMenuHtml($newArray);
        
    }

    public function setProductInCategories($dataArray){
        $newDataArray = $dataArray;
        foreach($dataArray as $key => $data){
            if(!empty($data["children"])){
                $newDataArray[$key]["children"] = $this->setProductInCategories($data["children"]);
                $newDataArray[$key]["children"] = array_merge($newDataArray[$key]["children"], $this->productClass->getList($data["id"]));
            } else {
                $newDataArray[$key]["children"] = $this->productClass->getList($data["id"]);
            }
        }
        return $newDataArray;
    }

    public function createMenuHtml($dataArray, $menuHtml = ""){
        foreach($dataArray as $data){
            if(!empty($data["children"])){
                if(isset($data["title"]) || !empty($data["related_product_id"])){
                    if($data["category_id"] < 1 && empty($data["related_product_id"])){
                        continue;
                    }

                    if(!empty($data["related_product_id"])){
                        $title = $data["name"];
                        $id = $data["related_product_id"];
                    } else {
                        $title = $data["title"];
                        $id = $data["id"];
                    }
                    $menuHtml .= "<li class='menu-item-has-children'><a href='product.php?" . $id . "'>" . $title . "</a>";
                } else {
                    $menuHtml .= "<li class='menu-item-has-children'><a href='category.php?" . $data["id"] . "'>" . $data["name"] . "</a>";
                }
                $menuHtml .="<ul class='sub-menu'>";
                $menuHtml .= $this->createMenuHtml($data["children"]);
                $menuHtml .= "</ul>";
                $menuHtml .= "</li>";
            } else {
                if(isset($data["title"]) || !empty($data["related_product_id"])){
                    if($data["category_id"] < 1 && empty($data["related_product_id"])){
                        continue;
                    }

                    if(!empty($data["related_product_id"])){
                        $title = $data["name"];
                        $id = $data["related_product_id"];
                    } else {
                        $title = $data["title"];
                        $id = $data["id"];
                    }
                    $menuHtml .= "<li class='menu-item-has-children'><a href='product.php?" . $id . "'>" . $title . "</a>";
                } else {
                    $menuHtml .= "<li><a href='category.php?" . $data["id"] . "'>" . $data["name"] . "</a></li>";
                }
            }
        }
        return $menuHtml;
    }
}

$utility = new Utility();