<?php
namespace Admin\Banners;

class Banners{

    public $table_name = "banners";

    public $id = "id";
    public $title = "title";
    public $description = "description";
    public $image = "image";
    public $status = "status";
    public $type = "type";

    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Pages Objects List
     */
    public function getList($bannerType = ""){
        global $mysqli;

        if(!empty($bannerType)){
            $condition = " WHERE type='" . $bannerType . "'";
        }
        $query = "SELECT * FROM $this->table_name" . $condition;
        
        if($result = $mysqli->query($query)){
           
            return $result->fetch_all(MYSQLI_ASSOC);
            
        } else {
            return false;
        }
    }
    
     /**
     * Developed By : Shivani
      * Date : 20-09-2020
     
     * Get Images
     */
    public function getImg($bannerType = ""){
        global $mysqli;

        if(!empty($bannerType)){
            $condition = " WHERE type='" . $bannerType . "'";
        }

        $query = "SELECT $this->image FROM $this->table_name" . $condition;
        if($result = $mysqli->query($query)){
            return $result->fetch_all(MYSQLI_ASSOC);
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $data
     * @return boolean
     * 
     * Insert banner data in DB
     */
    public function insert($data){
        global $mysqli;

        $columns = implode(',', array_keys($data));
        $values = '"' . implode('","', array_values($data)) . '"';
        $query = "INSERT INTO $this->table_name ($columns) VALUE ($values)";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update banner record in DB
     */
    public function update($recordId, $bannerData){
        global $mysqli;

        $count = 1;
        foreach($bannerData as $columnName => $columnValue){
            if(count($bannerData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE id=$recordId";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

    public function delete($id){
        global $mysqli;

        $query = "DELETE FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getTypesDropdownList() {
        global $mysqli;

        $bannerTypes = array(
            BANNER_TYPE_HOME => "Home",
            BANNER_TYPE_COMPANY_PROFILE => "About",
            BANNER_TYPE_PRODUCT => "Product",
            BANNER_TYPE_QUALITY_POLICY => "Quality Policy",
            BANNER_TYPE_INDUSTRIES => "Industries Served",
            BANNER_TYPE_TEAM => "Team",
            BANNER_TYPE_EVENTS => "Events",
            BANNER_TYPE_CERTIFICATES => "Certificates"
        );

        return $bannerTypes;
    }

}

$adminBannersClass = new Banners();