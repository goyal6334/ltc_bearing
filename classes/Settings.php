<?php
namespace Admin\Settings;

class Settings{

    public $table_name = "settings";
    public $id = "id";
    public $setting_key = "setting_key";
    public $setting_val = "setting_val";
    
    public $contactEmail = "contact_email";
    public $contactNumber = "contact_number";
    public $siteTitle = "site_title";
    public $logo_img = "logo_img";


    /**
     * 
     * @global type $mysqli
     * @return boolean
     * 
     * Get Gallery Objects List
     */
    public function getList(){
        global $mysqli;

        
        $query = "SELECT * FROM $this->table_name";
        if($result = $mysqli->query($query)){
           
            $settings = $result->fetch_all(MYSQLI_ASSOC);
            $returnData = array();           
            foreach($settings as $setting){
                $returnData[$setting['setting_key']] = $setting['setting_val'];
            }
            return $returnData;
        } else {
            return false;
        }
    }

   

    /**
     * 
     * @global \Admin\Banners\type $mysqli
     * @param type $recordId
     * @param type $bannerData
     * @return boolean
     * 
     * Update gallery record in DB
     */
    public function update($settingsData){
        global $mysqli;
       
        $count = 1;
        foreach($settingsData as $columnName => $columnValue){
            if(count($settingsData) > $count){
                $columnAndValues .= $columnName . '="' . $columnValue . '", ';
            } else {
                $columnAndValues .= $columnName . '="' . $columnValue . '"';
            }

            $count++;
        }

        $query = "UPDATE $this->table_name SET $columnAndValues WHERE setting_key=$settingsData";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function setSetting($settingKey, $settingVal){
        global $mysqli;
        
        $query = "UPDATE $this->table_name SET setting_val='$settingVal' WHERE setting_key='$settingKey'";
        if($result = $mysqli->query($query)){
            if($mysqli->affected_rows > 0){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getSetting($settingKey){
        global $mysqli;
      
        $query = "SELECT $this->setting_val FROM $this->table_name WHERE setting_key='".$settingKey."'";
        
        if($result = $mysqli->query($query)){
            $setting = $result->fetch_assoc();
            return $setting['setting_val'];
        } else {
            return false;
        }
    }

    public function getDetailsById($id){
        global $mysqli;

        $query = "SELECT * FROM $this->table_name WHERE id=" . $id;
        if($result = $mysqli->query($query)){
            return $result->fetch_array();
        } else {
            return false;
        }
    }

}

$adminSettingsClass = new Settings();