<?php
namespace Admin\User;

class User{
    
    /**
     * 
     * @param type $password
     * @return type
     * 
     * Hash password before save in DB
     */
    public function hashPassword($password){
        return password_hash($password, PASSWORD_DEFAULT, ['cost' => 12]);
    }

    /**
     * 
     * @param type $hashPassword
     * @param type $requestPassword
     * @return type
     * 
     * Verify hash password
     */
    public function verifyPassword($hashPassword, $requestPassword){
        return password_verify($requestPassword, $hashPassword);
    }

    /**
     * 
     * @param type $email
     * @return boolean
     * 
     * Verify email if found send user if not send false
     */
    public function verifyEmail($email){
        if($user = $this->getUserByEmail($email)){
            if(!empty($user)){
                return $user;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 
     * @global type $mysqli
     * @param type $email
     * @return boolean
     * 
     * Fetch user from email
     */
    public function getUserByEmail($email){
        global $mysqli;
        $query = "Select * from users where email='". $email ."' AND status=1";
        if($result = $mysqli->query($query)){
            return $result->fetch_object();
        } else {
            return false;
        }
    }

}

$adminUserClass = new User();