<?php include('common/header.php'); ?>

<!--<section class="noo-page-heading">
<div class="container">
<h1 class="page-title">Quality Policy</h1>
</div>
</section>-->
<div class="container-fluid">
        <div class="row row-fluid">
            <div data-aos="fade-right" class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("team") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>               
            </div>
        </div>
    </div>

<div class="main-content">
    <div class="container-fluid">
        <div class="row row-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 pt-2">
                        <h2 class="intro-title-1" data-aos="fade-right">Quality Policy</h2>
                        <h4 class="intro-title-2" data-aos="fade-left">Lakshmi Trading Company offers world-class bearings with international quality systems that ensure:-</h4>
                        <ul data-aos="fade-left">
                            <li>Selective sourcing of product</li>
                            <li>On-time delivery</li>
                            <li>Mistake prevention</li>
                            <li>Optimal operational costs</li>
                            <li>Customer feedback</li>
                        </ul>
                        <p class="text-justify" data-aos="fade-left">Team Lakshmi Trading Company is offered excellent in–house training for developing the necessary technical competence. Besides constant skill enhancement, the team is constantly exposed to advanced quality control initiatives including the use of IT</p>
                        <br>
                        <h4 class="intro-title-2" data-aos="fade-left">Lakshmi Trading Company Quality Commitment is reflected in:-</h4>
                        <ul data-aos="fade-left">
                            <li>Vibrant Corporate Culture</li>
                            <li>Professional Team</li>
                            <li>India's largest warehouse for bearingsn</li>
                            <li>Introduction of first mobile bearing service</li>
                        </ul>
                        <p class="text-justify" data-aos="fade-left">At <strong>Lakshmi Trading Company</strong>, we are committed to building value for our customers, our business, as well as our employees. We strive to conduct our business affairs with the highest ethical standards and work diligently to be a respected corporate citizen worldwide.
                            <br><br>
                            At the core of this effort is a corporate philosophy of highest possible standards when doing business. At the helm of affairs, our Directors have adopted Principles of Excellence that provide an effective corporate governance framework.
                            <br><br>
                            Lakshmi Trading Company relies on the diversity of its personnel, suppliers and valued customers to maximize innovation, growth, competitiveness, and above all customer satisfaction.
                            <br><br>
                            Lakshmi Trading Company is committed to delivering technology that provides solutions to varied needs of Indian Industry. We are honored to have been recognized by Indian Industry as the most reliable source for bearings in India.
                            <br><br>
                            As a growing organization we believe that we are indebted to the society to which we belong and are committed to contribute to its betterment and well-being.
                            <br><br>
                            Finally, we understand that as a company with an extensive network in India, we have a responsibility to continuously and consistently upgrade our knowledge, skills and work processes leading to a greater relationship with our customers, suppliers and community at large.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('common/footer.php'); ?>