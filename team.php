<?php include('common/header.php'); ?>
<div class="container-fluid">
        <div class="row row-fluid">
            <div class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("team") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>               
            </div>
        </div>
    </div>


<div class="main-content">
<div class="container-fluid">
<div class="row row-fluid pb-10">
<div class="container">
<div class="row">
<div class="col-sm-8">
<h2 class="intro-title-1" data-aos="fade-up">Our Team</h2>
<p data-aos="fade-right"> 
    Team Lakshmi Trading Company is offered excellent in–house 
    training for developing the necessary technical competence. Besides constant 
    skill enhancement, the team is constantly exposed to advanced 
    quality control initiatives including the use of IT.
    At the core of this effort is a corporate philosophy of highest possible standards when doing business. 
    At the helm of affairs, our Directors have adopted Principles of Excellence 
    that provide an effective corporate governance framework.

Lakshmi Trading Company relies on the diversity of its personnel, 
suppliers and valued customers to maximize innovation, growth, 
competitiveness, and above all customer satisfaction.

Lakshmi Trading Company is committed to delivering technology 
that provides solutions to varied needs of Indian Industry. 
We are honored to have been recognized by Indian Industry 
as the most reliable source for bearings in India.

    </p>
</div>
<br><br>
<div class="col-sm-4">
<img src="images/teaming.jpg" data-aos="fade-left" style="border-radius: 50%; margin-top: 80px;" class="img-responsive">
</div>
</div>
</div>
</div>
</div>


<div class="container-fluid">
<div class="row row-fluid pt-10 pb-10 bg-gray">
<div class="container">
<div class="row">
<div data-aos="fade-right" class="col-sm-12">
<div class="container">
<div class="row">
<div class="col-md-3 pull-rtl">
<div data-aos="flip-left" class="team-title">
<h3 class="intro-title-1">Team Members</h3>
<p> Lakshmi Trading Company is committed to delivering technology that provides solutions to varied needs of Indian Industry. </p>

</div>
</div>
<div class="col-md-9">
<div class="row">
<ul class="noo_team" data-aos="zoom-in-up">
<li>
<div class="noo-team-item">
<div class="noo-team-image">
<img width="212" height="328" src="images/t1.png"/>
<span class="icon"><span class="icon-square">
<a href="teamdetail.php" class="ajax-popup-link"><i class="fa fa-link"></i></a>
</span></span>
</div>
<div class="noo-team-info">
<div>
<h4 class="team_name">R.B Maurya</h4>
<span class="team_position">CEO</span>
</div>
</div>
</div>
</li>
<li>
<div class="noo-team-item">
<div class="noo-team-image">
<img width="212" height="328" src="images/t2.png"/>
<span class="icon"><span class="icon-square">
<a href="teamdetail.php" class="ajax-popup-link"><i class="fa fa-link"></i></a>
</span></span>
</div>
<div class="noo-team-info">
<div>
<h4 class="team_name">A.K Maurya</h4>
<span class="team_position">Head – Business Co-ordination</span>
</div>
</div>
</div>
</li>
<li>
<div class="noo-team-item">
<div class="noo-team-image">
<img width="212" height="328" src="images/t3.png"/>
<span class="icon"><span class="icon-square">
<a href="teamdetail.php" class="ajax-popup-link"><i class="fa fa-link"></i></a>
</span></span>
</div>
<div class="noo-team-info">
<div>
<h4 class="team_name">Mukund Maurya</h4>
<span class="team_position">Sr.Manager-Marketing</span>
</div>
</div>
</div>
</li>

</ul>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

</div>

<?php include('common/footer.php'); ?>


