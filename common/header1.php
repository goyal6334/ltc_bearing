<header class="noo-header header-2 header-style2">
<div class="navbar-wrapper">
<div class="navbar navbar-default">
<div class="noo-topbar">
<div class="container">
<ul class="pull-left">
<li> <a href="#"><i class="fa fa-phone"></i>+91-000-000-0000</a> </li>
<li> <a href="mailto:email@domain.com"> <i class="fa fa-envelope-o"></i>email@domain.com </a> </li>
</ul>
<ul class="pull-right">
<li> <a href="#"> <i class="fa fa-clock-o"></i> Working hours: Mon-Fri (9.00 am - 5.00 pm) </a> </li>
</ul>
</div>
</div>

<div class="noo-nav-header4">
<div class="container">
<div class="noo-nav-wrap">
<div class="navbar-header pull-left">
<h1 class="sr-only">LTC Bearing</h1>
<button data-target=".nav-collapse" class="btn-navbar noo_icon_menu" type="button">
<i class="fa fa-bars"></i>
</button>
<a href="index.php" class="navbar-brand"> <img class="noo-logo-img noo-logo-normal" src="images/logo.jpg" alt=""> </a>
</div>
</div>
</div>
</div>
<div class="noo-wrap-menu noo-wrap-menu2">
<div class="container">
<nav class="noo-main-menu pull-left">
<ul class="nav-collapse navbar-nav">
<li class="current-menu-item"><a href="index.php">Home</a></li>
<li class="menu-item-has-children"> <a href="#">About Us</a>
<ul class="sub-menu">
<li><a href="#">Company Profile</a></li>
<li><a href="#">Director's Message</a></li>
<li><a href="#">Why Us</a></li>
<li><a href="#">Quality Policy</a></li>
</ul>
</li>
<li class="menu-item-has-children"> <a href="#">Our Products</a>
<ul class="sub-menu">
<li><a href="#">INA</a></li>
<li><a href="#">CPC</a></li>
<li><a href="#">ABBA</a></li>
<li><a href="#">BD</a></li>
<li><a href="#">IKO</a></li>
<li><a href="#">LM SERIES</a></li>
<li><a href="#">KGB Plain Bearings</a></li>
<li><a href="#">Lubricating Bushes and Bearing</a></li>
</ul>
</li>
<li><a href="#">Industries Serve</a></li>
<li><a href="#">Clients</a></li>
<li><a href="#">Contact Us</a></li>
</ul>
</nav>
</div>
</div>
</div>
</div>
</header>