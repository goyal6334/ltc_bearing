<div class="section-type-b section-bg section-bg_light wow" style="background: linear-gradient(to right, rgba(255,0,0,0), rgba(0,137,61,1));">
<div class="section__inner">
<div class="container">
<div class="row">
<div class="col-sm-7" data-aos="fade-left">
<section class="b-type-e">
<h2 class="b-type-e__title ui-title-inner-1" style="color: #000; font-family: cursive;">
We Provide Best Services That Matches <br>
Your Needs &amp; Your Budget!
</h2>
</section>
</div>
<div class="col-sm-5">
<div class="b-contact-banner b-contact-banner_mod-a" data-aos="fade-right">
<div class="b-contact-banner__border"></div>
<div class="b-contact-banner__inner">
<h3 class="b-contact-banner__title">contact us for a competitive price</h3>
<div class="b-contact-banner__decor ui-decor-type-3 center-block"></div>
                            <div class="b-contact-banner__info b-contact-banner__info_lg"><?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactNumber); ?></div>
                            <div class="b-contact-banner__info"><?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactEmail); ?></div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>


<footer class="footer wow footer_mod-a">
<div class="footer__main footer__main_mod-a parallax-bg parallax-dark">
<ul class="bg-slideshow">
<li>
<div style="background-image:url(images/fbg.jpg)" class="bg-slide"></div>
</li>
</ul>
<div class="parallax__inner">
<div class="container">
<div class="row">
<div class="col-xs-12" data-aos="fade-down">
<section class="footer-banner">
<div class="footer-banner__decor"></div>
<div class="footer-banner__inner clearfix">
<div class="footer-banner__wrap">
<h3 class="footer-banner__title">Are you Interested in Our Products!</h3>
<div class="footer-banner__text"> We keep bearings in wide range useful for various industries & market.</div>
</div>
<a href="contact.php" class="footer-banner__btn btn btn-primary btn-effect">Contact Us</a> </div>
</section>
</div>
</div>
<div class="row">
<div data-aos="flip-left" class="col-md-4">
<a href="#" class="footer__logo"><img src="images/logo.jpg" class="img-responsive"/></a>
<div class="footer-form__info">
<p style="color:#fff;">Lakshmi Trading Company established by Mr. R.B Maurya 
    in the year 1994 is one of the largest importer & stockist of bearings. 
    Situated in the business capital of India. Delhi it carries very good name 
    & goodwill in the market. We keep bearings in wide range useful for various industries & market.
    </p>
</div>
</div>

<div data-aos="fade-left" class="col-md-2">
<section class="footer-section">
<h3 class="footer-section__title">Quick Links</h3>
<ul class="footer-section__list list list-mark-4">
<li><a href="index.php">Home</a></li>
<li><a href="company-profile.php">About Us</a></li>
<li><a href="#">Our Products</a></li>
<li><a href="industries-served.php">Industries Served</a></li>
<li><a href="team.php">Our Team</a></li>
<li><a href="events.php">Events</a></li>
<li><a href="certficate.php">Certificates</a></li>
<li><a href="contact.php">Contact Us</a></li>
</ul>
</section>
</div>

<div data-aos="fade-left" class="col-md-3">
<section class="footer-section">
<h3 class="footer-section__title">Our Products</h3>
<ul class="footer-section__list list list-mark-4">
<li><a href="#">INA</a></li>
<li><a href="#">CPC</a></li>
<li><a href="#">ABBA</a></li>
<li><a href="#">BD</a></li>
<li><a href="#">IKO</a></li>
<li><a href="#">LM SERIES</a></li>
<li><a href="#">KGB Plain Bearings</a></li>
<li><a href="#">Lubricating Bushes and Bearing</a></li>
</ul>
</section>
</div>
<div data-aos="fade-left" class="col-md-3">
<section class="footer-section">
<h3 class="footer-section__title">Contact US</h3>
<div class="footer__contact">T: <?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactNumber); ?></div>
<div class="footer__contact">E: <?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactEmail); ?></div>
<div class="footer__contact">A: 1519/3,GALI , AKHUNJI FRASH KHANA G.B.ROAD , DELHI,110006</div>
<ul class="social-net list-inline">
<li class="social-net__item"><a href="#" class="social-net__link"><i class="icon fa fa-twitter"></i></a></li>
<li class="social-net__item"><a href="#" class="social-net__link"><i class="icon fa fa-facebook"></i></a></li>
<li class="social-net__item"><a href="#" class="social-net__link"><i class="icon fa fa-linkedin"></i></a></li>
</ul>
</section>
</div>
</div>
</div>
</div>
</div>
<div class="copyright">
<div class="container">
<div class="row">
<div class="col-xs-12">Copyrights 2020. All rights reserved by <a class="copyright__link">LTC Bearing</a> Developed By Hands For IT</div>
</div>
</div>
</div>
</footer>
</div>


<script type='text/javascript' src='js/jquery.min.js'></script>
<script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?ver=1.0'></script>
<script type='text/javascript' src='js/bootstrap.min.js'></script>
<script type='text/javascript' src='js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='js/modernizr-2.7.1.min.js'></script>
<script type='text/javascript' src='js/jquery.cookie.js'></script>
<script type='text/javascript' src='js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='js/jquery.prettyPhoto.init.min.js'></script>
<script type='text/javascript' src='js/script.js'></script>
<script type='text/javascript' src='js/owl.carousel.min.js'></script>
<script type='text/javascript' src='js/jquery.parallax-1.1.3.js'></script>
<script type='text/javascript' src='js/jquery.magnific-popup.js'></script>

<script type='text/javascript' src='js/off-cavnass.js'></script>
<script type='text/javascript' src='js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='js/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.video.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.slideanims.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.actions.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.layeranimation.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.kenburn.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.navigation.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.migration.min.js'></script>
<script type='text/javascript' src='js/extensions/revolution.extension.parallax.min.js'></script>
<script src="js/aos.js"></script>
<script type='text/javascript' src='js/custom.js'></script>
<!--<script type="text/javascript" src="js/style.selector.js"></script>-->
 <script>
        AOS.init({
            duration: 1200,
            once: true,
        });

        $(document).ready(function() {

            if (window.location.href.indexOf("?msg=success") > -1) {
                $('#responseModal .modal-body p').text('Enquiry has been sent successfully!')
                $('#responseModal').modal('show');
            } else if (window.location.href.indexOf("?msg=c") > -1) {
                $('#responseModal .modal-body p').text('Something went wrong please try again!')
                $('#responseModal').modal('show');
            } else if (window.location.href.indexOf("?msg=captcha-invalid") > -1) {
                $('#responseModal .modal-body p').text('Captcha verification failed please try again!')
                $('#responseModal').modal('show');
            } else if (window.location.href.indexOf("?msg=captcha-notfound") > -1) {
                $('#responseModal .modal-body p').text('Please solve captcha first!')
                $('#responseModal').modal('show');
            }

            let flagCount = sessionStorage.getItem("flagCount");
            if (flagCount === null) {
                flagCount = 0;
            }
            if (flagCount === 0) {
                setTimeout(function() {
                    $('#enquiry-popup').modal('show');
                }, 10000);

                setTimeout(function() {
                    $('#enquiry-popup').modal('hide');
                }, 20000);
            }

            $('.closePopUp').click(function() {
                sessionStorage.setItem("flagCount", 1);
            });

            $('#contact-form').submit(function() {
                sessionStorage.setItem("flagCount", 1);
            });

            var offsetHeight = 90;
            $('#toggleBox').click(function() {
                if ($('#toggleBox> .toggle-icon> i').hasClass('fa-plus')) {
                    $('#toggleBox> .toggle-icon').empty();
                    $('#toggleBox> .toggle-icon').append("<i class = 'fa fa-minus'></i>");
                    $('#popForm').slideDown();
                } else {
                    $('#toggleBox> .toggle-icon').empty();
                    $('#toggleBox> .toggle-icon').append("<i class='fa fa-plus'></i>");
                    $('#popForm').slideUp();
                }
            });
            if (window.location.href.indexOf("#about") > -1) {
                var scrollPos = $('body> .main-container').find('#about').offset().top - (offsetHeight - 1);
                $('body,html').animate({
                    scrollTop: scrollPos
                }, 500, function() {
                    $(".btn-navbar").click();
                });
            }

            $('.scrollAbout').click(function(event) {
                var scrollPos = $('body> .main-container').find('#about').offset().top - (
                    offsetHeight - 1);
                $('body,html').animate({
                    scrollTop: scrollPos
                }, 500, function() {
                    $(".btn-navbar").click();
                });
                return false;
            });

        });

        $("#schaeffler").owlCarousel({
            autoplay: true,
            nav: true,
            loop: true,
            slideBy: 1,
            lazyLoad: true,
            margin: 0,
            /*
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            */
            touchDrag: false,
            mouseDrag: false,
            responsiveClass: true,
            smartSpeed: 1000,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },

                550: {
                    items: 2
                },

                991: {
                    items: 3
                },

                1024: {
                    items: 3
                },

                1366: {
                    items: 3
                }
            }
        });


        $("#home-product").owlCarousel({
            autoplay: true,
            nav: true,
            loop: true,
            slideBy: 1,
            lazyLoad: true,
            margin: 0,
            /*
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            */
            touchDrag: false,
            mouseDrag: false,
            responsiveClass: true,
            smartSpeed: 1000,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },

                550: {
                    items: 2
                },

                991: {
                    items: 3
                },

                1024: {
                    items: 3
                },

                1366: {
                    items: 4
                }
            }
        });

        $("#client").owlCarousel({
            autoplay: true,
            nav: true,
            loop: true,
            slideBy: 4,
            lazyLoad: true,
            margin: 0,
            /*
            animateOut: 'fadeOut',
            animateIn: 'fadeIn',
            */
            touchDrag: false,
            mouseDrag: false,
            responsiveClass: true,
            smartSpeed: 1000,
            nav: true,
            responsive: {
                0: {
                    items: 1
                },

                414: {
                    items: 2
                },

                550: {
                    items: 3
                },

                1024: {
                    items: 4
                },

                1366: {
                    items: 4
                }
            }
        });
    </script>
</body>
</html>
