<footer class="wrap-footer">
<div class="colophon wigetized">
<div class="container">
<div class="row">

<div class="col-md-4 col-sm-6 noo-footer-4">
<div class="widget widget_noo_infomation">
<div class="social-all"> <span>Follow us</span>
<a href="#" class="fa fa-facebook"></a>
<a href="#" class="fa fa-twitter"></a>
<a href="#" class="fa fa-youtube"></a>
<a href="#" class="fa fa-skype"></a>
</div>
<div class="noo-info-top">
<a href="#"><img src="images/logo.jpg"></a>
<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book</p>
</div>
<div class="noo-info-footer"> Copyrights 2020. All rights reserved by ltcbearing<br>
Design By <a href="http://www.onetouchwebsite.com/" style="color:#000;">One Touch Website</a></div>
</div>
</div>

<div class="noo-md-custom noo-footer-4">
<div class="widget widget_noo_services">
<h4 class="widget-title">Our Products</h4>
<ul>
<li><a href="#">INA</a></li>
<li><a href="#">CPC</a></li>
<li><a href="#">ABBA</a></li>
<li><a href="#">BD</a></li>
<li><a href="#">IKO</a></li>
<li><a href="#">LM SERIES</a></li>
<li><a href="#">KGB Plain Bearings</a></li>
<li><a href="#">Lubricating Bushes and Bearing</a></li>
</ul>
</div>
</div>

<div class="noo-md-custom noo-footer-4">
<div class="widget widget_noo_menu">
<h4 class="widget-title">Quick Links</h4>
<div class="noo-custom-menu">
<ul>
<li><a href="#">HOME</a></li>
<li><a href="#">ABOUT US</a></li>
<li><a href="#">INDUSTRIES SERVED</a></li>
<li><a href="#">CLIENTS</a></li>
<li><a href="#">CONTACT US</a></li>
<li><a href="#">COMPANY PROFILE</a></li>
<li><a href="#">DIRECTOR'S MESSAGE</a></li>
<li><a href="#">WHY US</a></li>
<li><a href="#">QUALITY POLICY</a></li>
</ul>
</div>
</div>
</div>

<div class="noo-md-custom noo-footer-4">
<div class="widget tweets-widget">
<h4 class="widget-title">Contact Us</h4>
<div class="recent-tweets">
<ul class="noo-infomation-attr">
<li> <span class="fa fa-map-marker infomation-left"></span><span>1519/3,GALI , AKHUNJI FRASH KHANA G.B.ROAD , DELHI,110006</span></li>
<li> <span class="fa fa-phone infomation-left"></span> <span>+91-0000000000</span> </li>
<li> <span class="fa fa-envelope infomation-left"></span> <span>info@example.com</span> </li>
</ul>
</div>
</div>
</div>


</div>
</div>
</div>
</footer>

<a href="#" class="go-to-top hidden-print"><i class="fa fa-angle-up"></i></a>