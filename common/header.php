<?php include_once("config.php") ?>
<!doctype html>
<html lang="en-US">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
        <title><?php echo $adminSettingsClass->getSetting($adminSettingsClass->siteTitle) ?></title>
        <link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all'/>
        <link rel='stylesheet' href='css/commerce.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/prettyPhoto.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/jquery.selectBox.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/font-awesome.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/owl.carousel.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/owl.theme.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/jquery.ui.timepicker.css' type='text/css' media='all' />
        <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:100,300,400,700,800,900,300italic,400italic,700italic,900italic' type='text/css' media='all' />
        <link rel='stylesheet' href='css/style.css' type='text/css' media='all' />
        <link rel='stylesheet' href='css/custom.css' type='text/css' media='all' />
        <link rel="stylesheet" href='css/magnific-popup.css' type='text/css' media='all' />
        <link rel="stylesheet" href='css/settings.css' type='text/css' media='all' />
        <link rel="stylesheet" href='css/preloader.css' type='text/css' media='all' />
        <link rel="stylesheet" href="css/style-selector.css" type='text/css' media='all' />
        <link id="style-main-color" rel="stylesheet" href="css/colors/default.css">

        <link id="style-main-color" rel="stylesheet" href="css/theme.css">
         <link href="css/aos.css" rel="stylesheet">
    </head>
    <body>

        <div class="site">
            <header class="noo-header header-2 header-style2 header-6">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-default">
                        <div class="container">
                            <div class="noo-nav-wrap">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="navbar-header">
                                            <button data-target=".nav-collapse" class="btn-navbar noo_icon_menu" type="button">
                                                <i class="fa fa-bars"></i>
                                            </button>
                                            
                                                 <?php if (!empty($adminSettingsClass->getSetting($adminSettingsClass->logo_img))) { ?>
                                                     <a href="index.php" class="navbar-brand navbar-center"><img class="noo-logo-img noo-logo-normal" data-aos="flip-right" src='<?php echo ADMIN_SETTINGS_UPLOAD_DIR_URL . $adminSettingsClass->getSetting($adminSettingsClass->logo_img); ?>'width="250"></a>
                                                 <?php } ?>
                                                
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="noo-infomation-cart pull-right" data-aos="flip-left">
                                            <ul>
                                                <li class="phone"><i class="fa fa-phone"></i><a href="tel:+91-000-000-0000"> Call us now <strong><?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactNumber); ?></strong></a></li>
                                                <li class="phone"><i class="fa fa-envelope"></i><a href="mailto:info@example.com"> Mail us <strong><?php echo $adminSettingsClass->getSetting($adminSettingsClass->contactEmail) ?></strong></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="noo-wrap-menu noo-wrap-menu2" data-aos="fade-left">
                            <nav class="noo-main-menu">
                                <ul class="nav-collapse navbar-nav nav-center">
                                    <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'index.php') != false)?"current-menu-item":"" ?>"><a href="index.php">Home</a></li>
                                    <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'company-profile.php') != false)?"current-menu-item":"" ?>"><a href="company-profile.php">About Us</a></li>
                                    <!--<li class="menu-item-has-children"> <a href="#">About Us</a>
                                    <ul class="sub-menu">
                                    <li><a href="company-profile.php">Company Profile</a></li>
                                    <li><a href="#">Director's Message</a></li>
                                    <li><a href="#">Why Us</a></li>
                                    <li><a href="#">Quality Policy</a></li>
                                    </ul>
                                    </li>-->
                                    <li class="menu-item-has-children"><a>Our Products</a>
                                        <ul class="sub-menu">
                                            <?php echo $utility->getMenus(); ?>
                                        </ul>
                                    </li>
                                </li>
                                <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'quality-policy.php') != false)?"current-menu-item":"" ?>"><a href="quality-policy.php">Quality Policy</a></li>
                                <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'industries-served.php') != false)?"current-menu-item":"" ?>"><a href="industries-served.php">Industries Served</a></li>
                                <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'team.php') != false)?"current-menu-item":"" ?>"><a href="team.php">Team</a></li>
                                <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'events.php') != false)?"current-menu-item":"" ?>"><a href="events.php">Events</a></li>
                                <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'certficate.php') != false)?"current-menu-item":"" ?>"><a href="certficate.php">Certificate</a></li>
                                <li class="<?php echo (strpos($_SERVER['REQUEST_URI'], 'contact.php') != false)?"current-menu-item":"" ?>"><a href="contact.php">Contact Us</a></li>
                                </ul>
                            </nav>
                        </div>

                    </div>
                </div>
            </header>
