<?php
session_start();

//Debug Mode
ini_set('display_errors', 1);

define('BASE_URL', 'http://localhost/ltc_bearing/');
define('ADMIN_BASE_URL', BASE_URL . 'admin/');
define('ROOT_DIR', $_SERVER['DOCUMENT_ROOT'] . '/ltc_bearing/');

//Banner Types
define('BANNER_TYPE_COMPANY_PROFILE', 'compony_profile');
define('BANNER_TYPE_HOME', 'home');
define('BANNER_TYPE_PRODUCT', 'product');
define('BANNER_TYPE_QUALITY_POLICY', 'quality_policy');
define('BANNER_TYPE_INDUSTRIES', 'industries');
define('BANNER_TYPE_TEAM', 'team');
define('BANNER_TYPE_EVENTS', 'events');
define('BANNER_TYPE_CERTIFICATES', 'certificates');

//Banner
define('ADMIN_BANNER_UPLOAD_DIR_PATH', ROOT_DIR . '/assets/uploads/banners/');
define('ADMIN_BANNER_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/banners/');

//Gallery
define('ADMIN_GALLERY_UPLOAD_DIR_PATH', ROOT_DIR . '/assets/uploads/gallery/');
define('ADMIN_GALLERY_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/gallery/');

//Clients
define('ADMIN_CLIENT_UPLOAD_DIR_PATH', ROOT_DIR . '/assets/uploads/clients/');
define('ADMIN_CLIENT_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/clients/');

//Download
define('ADMIN_DOWNLOAD_UPLOAD_DIR_PATH', ROOT_DIR . '/assets/uploads/download/');
define('ADMIN_DOWNLOAD_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/download/');


//Industry_Served
define('ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_PATH', ROOT_DIR . '/assets/uploads/industry_served/');
define('ADMIN_INDUSTRY_SERVED_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/industry_served/');

//Settings
define('ADMIN_SETTINGS_DIR_PATH', ROOT_DIR . '/assets/uploads/settings/');
define('ADMIN_SETTINGS_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/settings/');

//Certicates
define('ADMIN_CERTIFICATES_UPLOAD_DIR_PATH', ROOT_DIR . '/assets/uploads/certificates/');
define('ADMIN_CERTIFICATES_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/certificates/');

//Events
define('ADMIN_EVENTS_UPLOAD_DIR_PATH', ROOT_DIR . '/assets/uploads/events/');
define('ADMIN_EVENTS_UPLOAD_DIR_URL', BASE_URL . '/assets/uploads/events/');

include_once 'db_connection.php';

if ($mysqli->connect_errno) {
    echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
}

//PHP Mailer
include_once ROOT_DIR . 'php_mailer/PHPMailer.php';
include_once ROOT_DIR . 'php_mailer/SMTP.php';
include_once ROOT_DIR . 'php_mailer/Exception.php';

include_once ROOT_DIR . 'classes/ConfigCommon.php';
include_once ROOT_DIR . 'classes/Email.php';
include_once ROOT_DIR . 'classes/Settings.php';
include_once ROOT_DIR . 'classes/Banners.php';
include_once ROOT_DIR . 'classes/Certificates.php';
include_once ROOT_DIR . 'classes/Event.php';
include_once ROOT_DIR . 'classes/IndustryServed.php';
include_once ROOT_DIR . 'classes/Clients.php';
include_once ROOT_DIR . 'classes/Category.php';
include_once ROOT_DIR . 'classes/Product.php';
include_once ROOT_DIR . 'classes/Utility.php';
