<?php include('common/header.php'); ?>
<div class="container-fluid">
        <div class="row row-fluid">
            <div data-aos="fade-right" class="col-sm-12">
                <div class="rev_slider_wrapper fullwidthbanner-container">
                    <div id="rev_slider" class="rev_slider fullwidthabanner">
                        <ul>
                            <?php foreach($adminBannersClass->getImg("team") as $image){ ?>
                                <li data-transition="fade,parallaxtotop" data-slotamount="default,default" data-easein="default,default" data-easeout="default,default" data-masterspeed="default,default" data-thumb="<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>" data-rotate="0,0" data-saveperformance="off" data-title="Slide">
                                    <img src='<?php echo ADMIN_BANNER_UPLOAD_DIR_URL . $image['image']; ?>'  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>               
            </div>
        </div>
    </div>

<div id="main" class="main-content container">
    <div class="container-fluid">
        <div class="row row-fluid">
            <div class="col-sm-12">
                <h3 data-aos="fade-left" class="services-cat-title"><span class="cat-title-first"><span class="cat-title-last" style="color:#fff;">Upcoming Events</span></span></h3>
                <ul data-aos="fade-up-left" class="services-slider">
                    <?php foreach ($adminEventsClass->getEvent(upcomming) as $image) {?> 
                    <li class="slider-item">
                        <div class="services-item">
                            <div class="inner120">
                                <div class="inner120">
                                    <div class="services-background" data-image='<?php echo ADMIN_EVENTS_UPLOAD_DIR_URL . $image['image'];?>'>
                                        <div class="services-content">
                                            <div class="inner120">
                                                <div class="inner120">
                                                    <div class="services-entry">
                                                        <strong><?php echo $image['title'];?></strong>
                                                      <a class="button" ><?php echo 'Start Date : ' . $image['startdate'];?>
                                                        <br />
                                                        <?php echo ' End Date : ' . $image['enddate'];?>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<h3 class="services-title"><a href="#">Heading</a></h3>-->
                    </li>
                    <?php }?>
                </ul>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row row-fluid">
            <div class="col-sm-12">
                <h3 data-aos="fade-right" class="services-cat-title"><span class="cat-title-first"><span class="cat-title-last" style="color:#fff;">Past Events</span></span></h3>
                <ul data-aos="fade-up-right" class="services-slider">
                    <?php foreach ($adminEventsClass->getEvent(past) as $image) {?>
                    <li class="slider-item">
                        <div class="services-item">
                            <div class="inner120">
                                <div class="inner120">
                                    <div class="services-background" data-image='<?php echo ADMIN_EVENTS_UPLOAD_DIR_URL . $image['image'];?>'>
                                        <div class="services-content">
                                            <div class="inner120">
                                                <div class="inner120">
                                                    <div class="services-entry">
                                                        <strong><?php echo $image['title'];?></strong>
                                                      
                                                        <a class="button" ><?php echo 'Start Date : ' . $image['startdate'];?>
                                                        <br />
                                                        <?php echo ' End Date : ' . $image['enddate'];?>
                                                        </a>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--<h3 class="services-title"><a href="#">Heading</a></h3>-->
                    </li>
                    <?php }?>

                </ul>
            </div>
        </div>
    </div>
</div>

<?php include('common/footer.php'); ?>
